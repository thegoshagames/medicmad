package com.example.medicmad.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName


/*
Описание: Модель заказа
Дата создания: 10.04.2023
Автор: Георгий Хасанов
 */
@Keep
data class Order(
    @SerializedName("address") val address: String,
    @SerializedName("date_time") val dateTime: String,
    @SerializedName("phone") val phone: String,
    @SerializedName("comment") val comment: String,
    @SerializedName("audio_comment") val audio: String,
    @SerializedName("patients") val patients: List<OrderPatient>
)

/*
Описание: Модель пациента заказа
Дата создания: 10.04.2023
Автор: Георгий Хасанов
 */
@Keep
data class OrderPatient(
    @SerializedName("name") val name: String,
    @SerializedName("items") val patients: List<OrderItem>
)

/*
Описание: Модель предмета заказа
Дата создания: 10.04.2023
Автор: Георгий Хасанов
 */
@Keep
data class OrderItem(
    @SerializedName("catalog_id") val id: Int,
    @SerializedName("price") val price: String,
)

//{
//    "address": "string",
//    "date_time": "string",
//    "phone": "string",
//    "comment": "string",
//    "audio_comment": "string",
//    "patients": [
//    {
//        "name": "string",
//        "items": [
//        {
//            "catalog_id": 1,
//            "price": "string"
//        }
//        ]
//    }
//    ]
//}