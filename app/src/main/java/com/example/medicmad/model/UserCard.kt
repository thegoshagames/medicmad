package com.example.medicmad.model

import androidx.annotation.Keep
import androidx.compose.runtime.mutableStateListOf
import com.google.gson.annotations.SerializedName

/*
Описание: Модель карты пользователя
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@Keep
data class UserCard(
    @SerializedName("id") val id: Int = 0,
    @SerializedName("firstname") val firstname: String,
    @SerializedName("lastname") val lastname: String,
    @SerializedName("middlename") val middlename: String,
    @SerializedName("bith") val bith: String,
    @SerializedName("pol") val pol: String,
    @SerializedName("image") var image: String? = "",
    @SerializedName("userCart") var userCart: MutableList<CartItem>? = mutableStateListOf(),
)
