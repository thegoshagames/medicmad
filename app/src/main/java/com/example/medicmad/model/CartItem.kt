package com.example.medicmad.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

/*
Описание: Моедль элемента корзины
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@Keep
data class CartItem(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("price") val price: String,
    @SerializedName("count") var count: Int
)
