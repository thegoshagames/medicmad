package com.example.medicmad.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

/*
Описание: Модель блока "Акции и новости"
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@Keep
data class NewsItem(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("description") val description: String,
    @SerializedName("price") val price: String,
    @SerializedName("image") val image: String
)
