package com.example.medicmad.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

/*
Описание: Моедль элемента адреса
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@Keep
data class AddressItem(
    @SerializedName("addressText") val addressText: String,
    @SerializedName("latitude") val latitude: String,
    @SerializedName("longitude") val longitude: String,
    @SerializedName("altitude") val altitude: String,
    @SerializedName("flat") val flat: String,
    @SerializedName("enter") val enter: String,
    @SerializedName("floor") val floor: String,
    @SerializedName("phoneCodeText") val phoneCodeText: String,
    @SerializedName("addressName") val addressName: String
)
