package com.example.medicmad.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

/*
    Описание: Модель заказа приложения
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
@Keep
data class AppOrder(
    @SerializedName("address") val address: String,
    @SerializedName("time") val timeText: String,
    @SerializedName("selectedUsers") val selectedUsers: List<UserCard>,
    @SerializedName("phone") val phoneText: String,
    @SerializedName("comment") val comment: String,
)
