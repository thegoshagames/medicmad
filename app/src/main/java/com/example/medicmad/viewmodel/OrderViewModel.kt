package com.example.medicmad.viewmodel


import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.medicmad.common.ApiService
import com.example.medicmad.common.GeoApiService
import com.example.medicmad.model.Order
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.Priority
import kotlinx.coroutines.launch
import java.security.Provider

/*
Описание: Класс логики экрана создания заказа
Дата создания: 10.04.2023
Автор: Георгий Хасанов
 */
class OrderViewModel: ViewModel() {
    val address = MutableLiveData<String>()
    val lat = MutableLiveData<String>()
    val long = MutableLiveData<String>()
    val alt = MutableLiveData<String>()

    val errorMessage = MutableLiveData<String>()
    val responseCode = MutableLiveData<Int>()

    /*
    Описание: Метод поиска адреса по координатам
    Дата создания: 10.04.2023
    Автор: Георгий Хасанов
     */
    fun searchAddress(activity: Activity, mContext: Context, fusedLocationProviderClient: FusedLocationProviderClient) {
        errorMessage.value = null
        responseCode.value = null
        address.value = null
        lat.value = null
        long.value = null
        alt.value = null

        if (ActivityCompat.checkSelfPermission(
                mContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
        } else {
            fusedLocationProviderClient.getCurrentLocation(Priority.PRIORITY_HIGH_ACCURACY, null).addOnCompleteListener {
                if (it.isComplete) {
                    lat.value = it.result.latitude.toString()
                    long.value = it.result.longitude.toString()
                    alt.value = it.result.altitude.toString()

                    viewModelScope.launch {
                        try {
                            val apiService = GeoApiService.getInstance()

                            val json = apiService.searchAddress("${lat.value},${long.value}")

                            if (json.code() == 200) {
                                address.value = json.body()!!.asJsonArray[0].asJsonObject.get("display_name").toString()
                            } else {
                                errorMessage.value = json.code().toString()
                            }
                        } catch (e: Exception) {
                            errorMessage.value = e.message.toString()
                        }
                    }
                }
            }
        }
    }

    fun createOrder(token: String ,order: Order) {
        errorMessage.value = null
        responseCode.value = null

        viewModelScope.launch {
            try {
                val apiService = ApiService.getInstance()

                val json = apiService.createOrder("Bearer ${token.replace("\"", "")}", order)

                if (json.code() == 200) {
                    responseCode.value = 200
                } else {
                    errorMessage.value = json.code().toString()
                }
            } catch (e: Exception) {
                errorMessage.value = e.message.toString()
            }
        }
    }
}