package com.example.medicmad.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.medicmad.common.ApiService
import com.example.medicmad.model.UserCard
import kotlinx.coroutines.launch

/*
Описание: Класс логики экранов входа в аккаунт
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
class LoginViewModel: ViewModel() {
    val responseCode = MutableLiveData<Int>()
    val user = MutableLiveData<UserCard>()
    val token = MutableLiveData<String>()
    val errorMessage = MutableLiveData<String>()

    /*
    Описание: Метод отправки кода на почту
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    fun sendCode(email: String) {
        responseCode.value = null
        token.value = null
        errorMessage.value = null
        user.value = null

        viewModelScope.launch {
            try {
                val apiService = ApiService.getInstance()

                val json = apiService.sendCode(email)

                if (json.code() == 200) {
                    responseCode.value = 200
                } else {
                    errorMessage.value = json.code().toString()
                }
            } catch (e: Exception) {
                errorMessage.value = e.message.toString()
            }
        }
    }

    /*
    Описание: Метод проверки кода из почты
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    fun checkCode(email: String, code: String) {
        responseCode.value = null
        token.value = null
        errorMessage.value = null
        user.value = null

        viewModelScope.launch {
            try {
                val apiService = ApiService.getInstance()

                val json = apiService.checkCode(email, code)

                if (json.code() == 200) {
                    token.value = json.body()!!.get("token").toString()
                } else {
                    errorMessage.value = json.code().toString()
                }
            } catch (e: Exception) {
                errorMessage.value = e.message.toString()
            }
        }
    }

    /*
    Описание: Метод создания карты пациента
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    fun createCard(bearerToken: String, userCard: UserCard) {
        responseCode.value = null
        token.value = null
        errorMessage.value = null
        user.value = null

        viewModelScope.launch {
            try {
                val apiService = ApiService.getInstance()

                val json = apiService.createProfile("Bearer ${bearerToken.replace("\"", "")}", userCard)

                if (json.code() == 200) {
                    user.value = json.body()!!
                } else {
                    errorMessage.value = json.code().toString()
                }
            } catch (e: Exception) {
                errorMessage.value = e.message.toString()
            }
        }
    }
}