package com.example.medicmad.viewmodel

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.medicmad.common.ApiService
import com.example.medicmad.model.UserCard
import kotlinx.coroutines.launch

/*
Описание: Класс логики взаимодействия с картой пациента
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
class UserViewModel: ViewModel() {
    val errorMessage = MutableLiveData<String>()
    val responseCode = MutableLiveData<Int>()

    val user = MutableLiveData<UserCard>()

    val selectedImage = MutableLiveData<Bitmap>()
    val selectedVideo = MutableLiveData<String>()

    /*
    Описание: Метод создания карты пациента
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    fun updateCard(bearerToken: String, userCard: UserCard) {
        responseCode.value = null
        errorMessage.value = null
        user.value = null

        viewModelScope.launch {
            try {
                val apiService = ApiService.getInstance()

                val json = apiService.updateProfile("Bearer ${bearerToken.replace("\"", "")}", userCard)

                if (json.code() == 200) {
                    user.value = json.body()!!
                } else {
                    errorMessage.value = json.code().toString()
                }
            } catch (e: Exception) {
                errorMessage.value = e.message.toString()
            }
        }
    }
}