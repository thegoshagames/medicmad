package com.example.medicmad.viewmodel

import android.annotation.SuppressLint
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.medicmad.common.ApiService
import com.example.medicmad.model.CatalogItem
import com.example.medicmad.model.NewsItem
import com.example.medicmad.model.UserCard
import kotlinx.coroutines.launch

/*
Описание: Класс логики экрана "Анализы"
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@SuppressLint("MutableCollectionMutableState")
class HomeViewModel: ViewModel() {
    private val _news: MutableList<NewsItem> = mutableStateListOf()
    val news: List<NewsItem> by mutableStateOf(_news)

    private val _catalog: MutableList<CatalogItem> = mutableStateListOf()
    val catalog: List<CatalogItem> by mutableStateOf(_catalog)

    val errorMessage = MutableLiveData<String>()
    val responseCode = MutableLiveData<Int>()

    /*
    Описание: Метод получения новостей и акции с сервера
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    fun getNews() {
        _news.clear()
        errorMessage.value = null
        responseCode.value = null

        viewModelScope.launch {
            try {

                val apiService = ApiService.getInstance()

                val json = apiService.getNews()

                if (json.code() == 200) {
                    responseCode.value = 200

                    if (json.body() != null) {
                        _news.addAll(json.body()!!)
                    }
                } else {
                    errorMessage.value = json.code().toString()
                }
            } catch (e: Exception) {
                errorMessage.value = e.message.toString()
            }
        }
    }

    /*
    Описание: Метод получения каталога товаров
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    fun getCatalog() {
        _catalog.clear()
        errorMessage.value = null
        responseCode.value = null

        viewModelScope.launch {
        try {
                val apiService = ApiService.getInstance()

                val json = apiService.getCatalog()

                if (json.code() == 200) {
                    responseCode.value = 200

                    if (json.body() != null) {
                        _catalog.addAll(json.body()!!)
                    }
                } else {
                    errorMessage.value = json.code().toString()
                }
            } catch (e: Exception) {
                errorMessage.value = e.message.toString()
            }
        }
    }
}