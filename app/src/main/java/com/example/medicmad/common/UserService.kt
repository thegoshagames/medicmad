package com.example.medicmad.common

import android.content.SharedPreferences
import com.example.medicmad.model.UserCard
import com.google.gson.Gson
import com.google.gson.JsonArray

/*
Описание: Класс хранения данных о ползователях
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
class UserService {
    /*
    Описание: Метод сохранения данных о пользователе
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    fun saveUser(shared: SharedPreferences, userList: MutableList<UserCard>) {
        val json = Gson().toJson(userList)

        with(shared.edit()) {
            putString("userList", json)
            apply()
        }
    }

    /*
    Описание: Метод получения данных о пользователе
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    fun getUserList(shared: SharedPreferences): MutableList<UserCard> {
        val jsonArray = shared.getString("userList", "[]")

        val userListJson = Gson().fromJson(jsonArray, JsonArray::class.java)

        val userList = mutableListOf<UserCard>()
        for (user in userListJson) {
            val jsonUser = user.asJsonObject

            userList.add(
                UserCard(
                    jsonUser.get("id").asInt,
                    jsonUser.get("firstname").asString,
                    jsonUser.get("lastname").asString,
                    jsonUser.get("middlename").asString,
                    jsonUser.get("bith").asString,
                    jsonUser.get("pol").asString,
                    jsonUser.get("image") as String?
                )
            )
        }

        return userList
    }
}