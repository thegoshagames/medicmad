package com.example.medicmad.common

import android.content.SharedPreferences
import android.util.Log
import com.example.medicmad.model.AddressItem
import com.example.medicmad.model.CartItem
import com.example.medicmad.model.UserCard
import com.google.gson.Gson
import com.google.gson.JsonArray

/*
Описание: Класс хранения данных об адресах пользователей
Дата создания: 10.04.2023
Автор: Георгий Хасанов
 */
class AddressService {
    /*
    Описание: Метод сохранения данных адресов в локальное хранлище
    Дата создания: 10.04.2023
    Автор: Георгий Хасанов
     */
    fun saveAddressList(shared: SharedPreferences, addressList: MutableList<AddressItem>) {
        val json = Gson().toJson(addressList)

        with(shared.edit()) {
            putString("addressList", json)
            apply()
        }
    }

    /*
    Описание: Метод получения данных адресов из локального хранилища
    Дата создания: 10.04.2023
    Автор: Георгий Хасанов
     */
    fun getAddressList(shared: SharedPreferences): MutableList<AddressItem> {
        val jsonArray = shared.getString("addressList", "[]")

        val addressJson = Gson().fromJson(jsonArray, JsonArray::class.java)

        val addressList = mutableListOf<AddressItem>()
        for (item in addressJson) {
            val jsonAddressItem = item.asJsonObject

            addressList.add(
                AddressItem(
                    jsonAddressItem.get("addressText").toString(),
                    jsonAddressItem.get("latitude").toString(),
                    jsonAddressItem.get("longitude").toString(),
                    jsonAddressItem.get("altitude").toString(),
                    jsonAddressItem.get("flat").toString(),
                    jsonAddressItem.get("enter").toString(),
                    jsonAddressItem.get("floor").toString(),
                    jsonAddressItem.get("phoneCodeText").toString(),
                    jsonAddressItem.get("addressName").toString()
                )
            )
        }

        return addressList
    }
}