package com.example.medicmad.common

import com.example.medicmad.model.CatalogItem
import com.example.medicmad.model.NewsItem
import com.example.medicmad.model.Order
import com.example.medicmad.model.UserCard
import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

/*
Описание: Интерфейс класса связи приложения с сервером
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
interface ApiService {

    /*
    Описание: Метод отправки кода на почту
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    @Headers(
        "accept: application/json"
    )
    @POST("sendCode")
    suspend fun sendCode(@Header("email") email: String): Response<JsonObject>

    /*
    Описание: Метод проверки кода из почты
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    @Headers(
        "accept: application/json"
    )
    @POST("signin")
    suspend fun checkCode(@Header("email") email: String, @Header("code") code: String): Response<JsonObject>

    /*
    Описание: Метод проверки кода из почты
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    @Headers(
        "accept: application/json",
        "Content-Type: application/json"
    )
    @POST("createProfile")
    suspend fun createProfile(@Header("Authorization") token: String, @Body userInfo: UserCard): Response<UserCard>

    /*
    Описание: Метод обновления карты пользователя
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    @Headers(
        "accept: application/json",
        "Content-Type: application/json"
    )
    @PUT("updateProfile")
    suspend fun updateProfile(@Header("Authorization") token: String, @Body userInfo: UserCard): Response<UserCard>

    /*
    Описание: Метод получения акций и новостей
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    @Headers(
        "accept: application/json"
    )
    @GET("news")
    suspend fun getNews(): Response<List<NewsItem>>

    /*
    Описание: Метод получения каталога товаров
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    @Headers(
        "accept: application/json"
    )
    @GET("catalog")
    suspend fun getCatalog(): Response<List<CatalogItem>>

    /*
    Описание: Метод отправки заказа на сервер
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    @Headers(
        "accept: application/json",
        "Content-Type: application/json"
    )
    @GET("order")
    suspend fun createOrder(@Header("Authorization") token: String, @Body order: Order): Response<JsonObject>

    companion object {
        var apiService: ApiService? = null

        /*
        Описание: Метод инициализации класса связи приложения с сервером
        Дата создания: 04.04.2023
        Автор: Георгий Хасанов
         */
        fun getInstance(): ApiService {
            if (apiService == null) {
                apiService = Retrofit.Builder()
                    .baseUrl("https://medic.madskill.ru/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(ApiService::class.java)
            }

            return apiService!!
        }
    }
}