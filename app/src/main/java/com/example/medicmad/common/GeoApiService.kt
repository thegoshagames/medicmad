package com.example.medicmad.common

import com.example.medicmad.model.CatalogItem
import com.example.medicmad.model.NewsItem
import com.example.medicmad.model.UserCard
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

/*
Описание: Интерфейс класса связи приложения с сервером геокодирования
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
interface GeoApiService {
    /*
    Описание: Метод получения адреса по координатам
    Дата создания: 10.04.2023
    Автор: Георгий Хасанов
     */
    @GET("search")
    suspend fun searchAddress(@Query("q") query: String, @Query("format") format: String = "json"): Response<JsonArray>

    companion object {
        var apiService: GeoApiService? = null

        /*
        Описание: Метод инициализации класса связи приложения с сервером геокодирования
        Дата создания: 04.04.2023
        Автор: Георгий Хасанов
         */
        fun getInstance(): GeoApiService {
            if (apiService == null) {
                apiService = Retrofit.Builder()
                    .baseUrl("https://nominatim.openstreetmap.org/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(GeoApiService::class.java)
            }

            return apiService!!
        }
    }
}