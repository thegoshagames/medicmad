package com.example.medicmad.common

import android.content.SharedPreferences
import android.util.Log
import com.example.medicmad.model.CartItem
import com.example.medicmad.model.UserCard
import com.google.gson.Gson
import com.google.gson.JsonArray

/*
Описание: Класс хранения данных о ползователях
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
class CartService {
    /*
    Описание: Метод сохранения данных корзины
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    fun saveCart(shared: SharedPreferences, cart: MutableList<CartItem>) {
        val json = Gson().toJson(cart)

        with(shared.edit()) {
            putString("cart", json)
            apply()
        }

        Log.d("TAG", "saveCart: ${cart.size}")
    }

    /*
    Описание: Метод получения данных из корзины
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    fun getCart(shared: SharedPreferences): MutableList<CartItem> {
        val jsonArray = shared.getString("cart", "[]")

        val cartJson = Gson().fromJson(jsonArray, JsonArray::class.java)

        val cart = mutableListOf<CartItem>()
        for (cartItem in cartJson) {
            val jsonCartItem = cartItem.asJsonObject

            cart.add(
                CartItem(
                    jsonCartItem.get("id").asInt,
                    jsonCartItem.get("name").asString,
                    jsonCartItem.get("price").asString,
                    jsonCartItem.get("count").asInt
                )
            )
        }

        return cart
    }
}