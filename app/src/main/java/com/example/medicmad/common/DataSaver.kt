package com.example.medicmad.common

import android.app.Application
import android.util.Log
import androidx.compose.runtime.key
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys

/*
Описание: Класс сохранения данных в зашифрованный Shared Preferences
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
class DataSaver(
    application: Application
) {
    private val mainKeyAlias by lazy {
        val keyGenParam = MasterKeys.AES256_GCM_SPEC
        MasterKeys.getOrCreate(keyGenParam)
    }

    private val eSharedPreferences = EncryptedSharedPreferences.create(
        "shared",
        mainKeyAlias,
        application,
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
    )

    /*
    Описание: Метод получения данных из зашифрованного Shared Preferences
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    fun saveStringToSharedPreferences(key: String, value: String) {
        with(eSharedPreferences.edit()) {
            putString(key, value)
            apply()
        }
    }

    /*
    Описание: Метод сохранения данных в зашифрованный Shared Preferences
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    fun getStringFromSharedPreferences(key: String): String {
        return eSharedPreferences.getString(key, "").toString()
    }
}