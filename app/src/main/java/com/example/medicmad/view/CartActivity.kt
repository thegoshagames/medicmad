package com.example.medicmad.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.medicmad.R
import com.example.medicmad.common.CartService
import com.example.medicmad.model.CartItem
import com.example.medicmad.view.ui.components.AppBackButton
import com.example.medicmad.view.ui.components.AppButton
import com.example.medicmad.view.ui.components.CatalogCartBlock
import com.example.medicmad.view.ui.theme.MedicMADTheme
import com.example.medicmad.view.ui.theme.iconColor
import com.example.medicmad.view.ui.theme.sourceSans

/*
Описание: Класс экрана корзины
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
class CartActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MedicMADTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    CartContent()
                }
            }
        }
    }

    /*
    Описание: Контент экрана корзины
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    @Composable
    fun CartContent() {
        val mContext = LocalContext.current
        val shared = this.getSharedPreferences("shared", Context.MODE_PRIVATE)

        var cart: MutableList<CartItem> = remember { mutableStateListOf() }

        LaunchedEffect(Unit) {
            cart.addAll(CartService().getCart(shared))
        }

        Scaffold(
            topBar = {
                AppBackButton(
                    modifier = Modifier.padding(start = 20.dp, top = 16.dp, bottom = 24.dp)
                ) {
                    val intent = Intent(mContext, HomeActivity::class.java)
                    startActivity(intent)
                }
            }
        ) { padding ->
            Box(modifier = Modifier.padding(padding)) {
                Column(modifier = Modifier
                    .fillMaxWidth()) {
                    Row(
                        horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 20.dp)
                    ) {
                        Text(
                            text = "Корзина",
                            fontSize = 24.sp,
                            fontWeight = FontWeight.Bold,
                            fontFamily = sourceSans
                        )
                        Icon(
                            painter = painterResource(id = R.drawable.ic_remove),
                            contentDescription = "",
                            tint = iconColor,
                            modifier = Modifier.clickable {
                                cart.clear()

                                CartService().saveCart(shared, cart)
                            }
                        )
                    }
                    LazyColumn(modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 20.dp)
                    ) {
                        item {
                            Spacer(modifier = Modifier.height(32.dp))
                        }
                        items(cart.distinct()) { item ->
                            CatalogCartBlock(
                                cartItem = item,
                                onItemDelete = {
                                    cart.remove(item)

                                    CartService().saveCart(shared, cart)
                                },
                                onItemPlus = {
                                    val itemIndex = cart.indexOfFirst { it.id == item.id }

                                    if (itemIndex != -1) {
                                        cart.removeAt(itemIndex)

                                        cart.add(
                                            itemIndex,
                                            CartItem(
                                                item.id,
                                                item.name,
                                                item.price,
                                                item.count + 1
                                            )
                                        )

                                        CartService().saveCart(shared, cart)
                                    }
                                },
                                onItemMinus = {
                                    val itemIndex = cart.indexOfFirst { it.id == item.id }

                                    if (itemIndex != -1) {
                                        cart.removeAt(itemIndex)

                                        cart.add(
                                            itemIndex,
                                            CartItem(
                                                item.id,
                                                item.name,
                                                item.price,
                                                item.count - 1
                                            )
                                        )

                                        CartService().saveCart(shared, cart)
                                    }
                                }
                            )
                            Spacer(modifier = Modifier.height(16.dp))
                        }
                        item {
                            Spacer(modifier = Modifier.height(40.dp))
                        }
                        item {
                            Row(
                                horizontalArrangement = Arrangement.SpaceBetween,
                                verticalAlignment = Alignment.CenterVertically,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(horizontal = 20.dp).padding(bottom = 100.dp)
                            ) {
                                var cartPrice = 0

                                for (cartItem in cart.distinct()) {
                                    cartPrice += cartItem.price.toInt() * cartItem.count
                                }

                                Text(
                                    text = "Сумма",
                                    fontSize = 20.sp,
                                    fontWeight = FontWeight.SemiBold,
                                    fontFamily = sourceSans
                                )
                                Text(
                                    text = "$cartPrice ₽",
                                    fontSize = 20.sp,
                                    fontWeight = FontWeight.SemiBold,
                                    fontFamily = sourceSans
                                )
                            }
                        }
                    }
                }
            }

            Box(modifier = Modifier
                .fillMaxSize()
                .padding(horizontal = 20.dp, vertical = 32.dp)) {
                AppButton(
                    modifier = Modifier.fillMaxWidth().align(Alignment.BottomCenter),
                    text = "Перейти к оформлению заказа"
                ) {
                    val intent = Intent(mContext, OrderActivity::class.java)
                    startActivity(intent)
                }
            }
        }
    }
}