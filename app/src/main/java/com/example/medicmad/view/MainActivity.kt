package com.example.medicmad.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.example.medicmad.common.DataSaver
import com.example.medicmad.view.ui.theme.MedicMADTheme
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/*
Описание: Класс Splash экрана
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
class MainActivity : ComponentActivity() {
    @SuppressLint("CoroutineCreationDuringComposition")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val mContext = LocalContext.current
            val shared = this.getSharedPreferences("shared", Context.MODE_PRIVATE)

            val splashScreen = installSplashScreen()
            splashScreen.setKeepOnScreenCondition { true }

            MedicMADTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                }
            }

            val scope = rememberCoroutineScope()

            scope.launch {
                delay(1000)

                if (shared.getBoolean("isFirstEnter", true)) {
                    val intent = Intent(mContext, OnboardActivity::class.java)
                    startActivity(intent)
                } else {
                    if (DataSaver(application).getStringFromSharedPreferences("token") != "") {
                        if (DataSaver(application).getStringFromSharedPreferences("password") != "") {
                            val intent = Intent(mContext, CreateCardActivity::class.java)
                            startActivity(intent)
                        } else {
                            val intent = Intent(mContext, PasswordActivity::class.java)
                            startActivity(intent)
                        }
                    } else {
                        val intent = Intent(mContext, LoginActivity::class.java)
                        startActivity(intent)
                    }
                }
            }
        }
    }
}