package com.example.medicmad.view.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.medicmad.model.CatalogItem
import com.example.medicmad.view.ui.theme.descriptionColor
import com.example.medicmad.view.ui.theme.primaryColor
import com.example.medicmad.view.ui.theme.sourceSans

/*
Описание: Элемент экрана поиска товаров
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@Composable
fun SearchComponent(
    catalogItem: CatalogItem,
    searchText: String
) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .fillMaxWidth()
            .drawBehind {
                drawLine(
                    descriptionColor.copy(0.2f),
                    Offset(0f, size.height),
                    Offset(size.width, size.height),
                    Stroke.DefaultMiter
                )
            }
            .padding(horizontal = 20.dp, vertical = 12.dp)
    ) {
        val text = "${catalogItem.name} (${catalogItem.bio})".split(searchText, ignoreCase = true)

        Text(
            text = buildAnnotatedString {
                for ((index, t) in text.withIndex()) {
                    append(t)

                    if (index != text.size - 1) {
                        withStyle(SpanStyle(color = primaryColor)) {
                            append(searchText)
                        }
                    }
                }
            },
            fontSize = 15.sp,
            color = Color.Black,
            fontFamily = sourceSans,
            modifier = Modifier.fillMaxWidth(0.6f)
        )

        Column(horizontalAlignment = Alignment.End) {
            Text(
                text = "${catalogItem.price} ₽",
                fontSize = 17.sp,
                fontFamily = sourceSans
            )
            Spacer(modifier = Modifier.height(1.dp))
            Text(
                text = "${catalogItem.time_result} ₽",
                fontSize = 14.sp,
                fontFamily = sourceSans,
                color = descriptionColor
            )
        }
    }
}