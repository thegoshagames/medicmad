package com.example.medicmad.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModelProvider
import com.example.medicmad.R
import com.example.medicmad.common.DataSaver
import com.example.medicmad.view.ui.components.AppTextButton
import com.example.medicmad.view.ui.theme.*
import com.example.medicmad.viewmodel.LoginViewModel

/*
Описание: Класс экрана создания пароля
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
class PasswordActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MedicMADTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    PasswordContent()
                }
            }
        }
    }

    /*
    Описание: Контент экрана создания пароля
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    @Composable
    fun PasswordContent() {
        val mContext = LocalContext.current

        var password by rememberSaveable { mutableStateOf("") }

        Scaffold(
            topBar = {
                Row(
                    horizontalArrangement = Arrangement.End,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(end = 20.dp, top = 40.dp, bottom = 40.dp)
                ) {
                    AppTextButton(
                        text = "Пропустить",
                        fontSize = 15.sp,
                        fontWeight = FontWeight.Normal,
                        fontFamily = sourceSans,
                        color = primaryColor,

                    ) {
                        val intent = Intent(mContext, CreateCardActivity::class.java)
                        startActivity(intent)
                    }
                }
            }
        ) { paddingValues ->
            Box(modifier = Modifier.padding(paddingValues)) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(40.dp)
                ) {
                    Text(
                        "Создайте пароль",
                        fontSize = 24.sp,
                        fontWeight = FontWeight.Bold,
                        fontFamily = sourceSans,
                        textAlign = TextAlign.Center
                    )
                    Spacer(modifier = Modifier.height(16.dp))
                    Text(
                        text = "Для защиты ваших персональных данных",
                        fontSize = 15.sp,
                        fontFamily = sourceSans,
                        color = descriptionColor,
                        textAlign = TextAlign.Center
                    )
                    Spacer(modifier = Modifier.height(56.dp))
                    Row(
                        horizontalArrangement = Arrangement.Center,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        for (i in 0..3) {
                            Box(
                                modifier = Modifier
                                    .padding(6.dp)
                                    .size(16.dp)
                                    .clip(CircleShape)
                                    .background(if (password.length > i) primaryColor else Color.White)
                                    .border(1.dp, primaryColor, CircleShape)
                            )
                        }
                    }
                    Spacer(modifier = Modifier.height(56.dp))
                    LazyVerticalGrid(
                        columns = GridCells.Fixed(3),
                        horizontalArrangement = Arrangement.spacedBy(24.dp),
                        verticalArrangement = Arrangement.spacedBy(24.dp),
                        modifier = Modifier.size(288.dp, 392.dp),
                        content = {
                            items(12) {
                                if (it == 11) {
                                    Box(
                                        modifier = Modifier
                                            .size(80.dp)
                                            .clip(CircleShape)
                                            .clickable {
                                                if (password.isNotEmpty()) {
                                                    password = password.substring(0, password.length - 1)
                                                }
                                            }
                                    ) {
                                        Icon(
                                            painter = painterResource(id = R.drawable.ic_delete),
                                            contentDescription = "",
                                            tint = Color.Black,
                                            modifier = Modifier.align(Alignment.Center)
                                        )
                                    }
                                } else if (it == 9) {

                                } else {
                                    Box(
                                        modifier = Modifier
                                            .size(80.dp)
                                            .clip(CircleShape)
                                            .background(inputColor)
                                            .clickable {
                                                if (password.length < 4) {
                                                    if (it == 10) {
                                                        password += "0"
                                                    } else if (it == 9) {

                                                    } else {
                                                        password += "${it + 1}"
                                                    }

                                                    if (password.length == 4) {
                                                        DataSaver(application).saveStringToSharedPreferences("password", password)

                                                        val intent = Intent(mContext, CreateCardActivity::class.java)
                                                        startActivity(intent)
                                                    }
                                                }
                                            }
                                    ) {
                                        Text(
                                            text = if (it != 10) "${it + 1}" else "0",
                                            fontSize = 24.sp,
                                            fontWeight = FontWeight.SemiBold,
                                            fontFamily = sourceSans,
                                            textAlign = TextAlign.Center,
                                            modifier = Modifier.align(Alignment.Center)
                                        )
                                    }
                                }
                            }
                        }
                    )
                }
            }
        }
    }
}