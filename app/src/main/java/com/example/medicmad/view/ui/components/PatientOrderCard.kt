package com.example.medicmad.view.ui.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.medicmad.R
import com.example.medicmad.model.CartItem
import com.example.medicmad.model.UserCard
import com.example.medicmad.view.ui.theme.*

/*
Описание: Компонент пользователя в заказе (если пользователей > 1)
Дата создания: 10.04.2023
Автор: Георгий Хасанов
 */
@Composable
fun PatientOrderCard(
    cart: List<CartItem>,
    user: UserCard,
    onUserSave: (UserCard) -> Unit,
    onUserChange: (UserCard) -> Unit,
    onUserRemove: (UserCard) -> Unit
) {
    Card(
        elevation = 0.dp,
        backgroundColor = Color.White,
        border = BorderStroke(1.dp, strokeColor),
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 16.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
                .padding(top = 24.dp)
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxWidth()
            ) {
                AppTextField(
                    value = "${user.lastname} ${user.firstname}",
                    onValueChange = {},
                    readOnly = true,
                    placeholder = {
                        Text(
                            text = "",
                            fontSize = 14.sp,
                            fontFamily = sourceSans,
                            fontWeight = FontWeight.Normal,
                            color = descriptionColor
                        )
                    },
                    leadingIcon = {
                        Image(
                            painter = if (user.pol == "Мужской") painterResource(id = R.drawable.ic_male) else painterResource(id = R.drawable.ic_female),
                            contentDescription = "",
                            modifier = Modifier.size(24.dp)
                        )
                    },
                    trailingIcon = {
                        Icon(
                            painter = painterResource(id = R.drawable.ic_drop),
                            contentDescription = "",
                            tint = textColor,
                            modifier = Modifier.clickable {
                                onUserChange(user)
                            }
                        )
                    },
                    modifier = Modifier.fillMaxWidth(0.8f)
                )
                Icon(
                    painter = painterResource(id = R.drawable.ic_dismiss),
                    contentDescription = "",
                    tint = iconColor,
                    modifier = Modifier
                        .size(10.dp)
                        .clickable { onUserRemove(user) }
                )
            }
            Spacer(modifier = Modifier.height(24.dp))
            for (item in cart.distinct()) {
                var checked by rememberSaveable { mutableStateOf(true) }

                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(bottom = 16.dp)
                ) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Checkbox(
                            checked = checked,
                            colors = CheckboxDefaults.colors(checkedColor = primaryColor, uncheckedColor = inputColor),
                            onCheckedChange = { check ->
                                checked = check

                                if (check) {
                                    val itemIndex = user.userCart!!.indexOfFirst { it.id == item.id }

                                    if (itemIndex != -1) {
                                        user.userCart!!.removeAt(itemIndex)
                                        user.userCart!!.add(
                                            itemIndex,
                                            CartItem(
                                                item.id,
                                                item.name,
                                                item.price,
                                                1
                                            )
                                        )

                                        onUserSave(user)
                                    }
                                } else {
                                    val itemIndex = user.userCart!!.indexOfFirst { it.id == item.id }

                                    if (itemIndex != -1) {
                                        user.userCart!!.removeAt(itemIndex)
                                        user.userCart!!.add(
                                            itemIndex,
                                            CartItem(
                                                item.id,
                                                item.name,
                                                item.price,
                                                0
                                            )
                                        )

                                        onUserSave(user)
                                    }
                                }
                            }
                        )
                        Spacer(modifier = Modifier.width(16.dp))
                        Text(
                            text = item.name,
                            fontSize = 12.sp,
                            fontFamily = sourceSans,
                            color = if (checked) Color.Black else descriptionColor,
                            softWrap = true,
                            modifier = Modifier.fillMaxWidth(0.7f)
                        )
                    }
                    Text(
                        text = "${item.price} ₽",
                        fontSize = 15.sp,
                        fontFamily = sourceSans,
                        color = if (checked) Color.Black else descriptionColor
                    )
                }
            }
            Spacer(modifier = Modifier.height(8.dp))
        }
    }
}