package com.example.medicmad.view

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.lifecycle.ViewModelProvider
import com.example.medicmad.R
import com.example.medicmad.view.ui.components.AppButton
import com.example.medicmad.view.ui.components.AppTextField
import com.example.medicmad.view.ui.theme.*
import com.example.medicmad.viewmodel.LoginViewModel

/*
Описание: Класс экрана входа в аккаунт
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
class LoginActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MedicMADTheme {
                Surface(
                    modifier = Modifier
                        .fillMaxSize()
                        .testTag("login"),
                    color = MaterialTheme.colors.background
                ) {
                    LoginContent()
                }
            }
        }
    }

    @Composable
    fun LoginContent() {
        val mContext = LocalContext.current

        val viewModel = ViewModelProvider(this)[LoginViewModel::class.java]

        var emailText by rememberSaveable { mutableStateOf("") }

        var enabled by rememberSaveable { mutableStateOf(false) }
        
        var isLoading by rememberSaveable { mutableStateOf(false) }
        var isAlertVisible by rememberSaveable { mutableStateOf(false) }

        val response by viewModel.responseCode.observeAsState()
        LaunchedEffect(response) {
            isLoading = false

            if (response == 200) {
                val intent = Intent(mContext, CodeActivity::class.java)
                intent.putExtra("email", emailText)

                startActivity(intent)
            }
        }

        val errorMessage by viewModel.errorMessage.observeAsState()
        LaunchedEffect(errorMessage) {
            isLoading = false

            if (errorMessage != null) {
                isAlertVisible = true
            }
        }
        
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .widthIn(max = 400.dp)
                .fillMaxSize()
        ) {
            Column(
                modifier = Modifier
                    .widthIn(max = 400.dp)
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp)
            ) {
                Row(modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 60.dp)) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_hello),
                        contentDescription = "",
                        modifier = Modifier.size(32.dp)
                    )
                    Spacer(modifier = Modifier.width(16.dp))
                    Text(
                        text = "Добро пожаловать!",
                        fontSize = 24.sp,
                        fontWeight = FontWeight.Bold,
                        fontFamily = sourceSans
                    )
                }
                Spacer(modifier = Modifier.heightIn(23.dp))
                Text(
                    text = "Войдите, чтобы пользоваться функциями приложения",
                    fontSize = 15.sp,
                    fontWeight = FontWeight.Normal,
                    fontFamily = sourceSans
                )
                Spacer(modifier = Modifier.heightIn(60.dp))
                Text(
                    text = "Вход по E-mail",
                    fontSize = 14.sp,
                    fontWeight = FontWeight.Normal,
                    fontFamily = sourceSans,
                    color = textColor
                )
                Spacer(modifier = Modifier.height(4.dp))
                AppTextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = emailText,
                    onValueChange = {
                        emailText = it

                        enabled = emailText.isNotEmpty()
                    },
                    placeholder = {
                        Text(
                            "example@mail.ru",
                            color = Color.Black.copy(0.5f),
                            fontSize = 15.sp,
                            fontFamily = sourceSans
                        )
                    }
                )
                Spacer(modifier = Modifier.height(34.dp))
                AppButton(
                    modifier = Modifier.fillMaxWidth(),
                    text = "Далее",
                    enabled = enabled
                ) {
                    if (Regex("^[a-z0-9]*@[a-z0-9]*\\.[a-z0-9]*$").matches(emailText)) {
                        isLoading = true

                        viewModel.sendCode(emailText)
                    } else {
                        viewModel.errorMessage.value = "Неправильный формат E-Mail!"
                    }
                }
            }
        }

        Box(
            modifier = Modifier
                .widthIn(max = 440.dp)
                .fillMaxSize()
        ) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .widthIn(max = 400.dp)
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp, vertical = 56.dp)
                    .align(Alignment.BottomCenter)
            ) {
                Text(
                    text = "Или войдите с помощью",
                    fontSize = 15.sp,
                    fontFamily = sourceSans,
                    color = descriptionColor,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(16.dp))
                AppButton(
                    modifier = Modifier.fillMaxWidth(),
                    text = "Войти с Яндекс",
                    fontSize = 17.sp,
                    fontWeight = FontWeight.Normal,
                    fontFamily = sourceSans,
                    color = Color.Black,
                    borderStroke = BorderStroke(1.dp, strokeColor),
                    contentPadding = PaddingValues(18.dp),
                    colors = ButtonDefaults.buttonColors(backgroundColor = Color.White)
                ) {

                }
            }
        }
        
        if (isLoading) {
            Dialog(onDismissRequest = {}) {
                CircularProgressIndicator()
            }
        }
        
        if (isAlertVisible) {
            AlertDialog(
                onDismissRequest = {
                    isAlertVisible = false
                    viewModel.errorMessage.value = null
                                   },
                title = { Text("Ошибка") },
                text = { Text(viewModel.errorMessage.value.toString()) },
                buttons = {
                    TextButton(onClick = {
                        isAlertVisible = false
                        viewModel.errorMessage.value = null
                    }) {
                        Text(text = "OK")
                    }
                }
            )
        }
    }
}