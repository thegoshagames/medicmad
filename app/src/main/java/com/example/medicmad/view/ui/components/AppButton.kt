package com.example.medicmad.view.ui.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.medicmad.R
import com.example.medicmad.view.ui.theme.*

/*
Описание: Текстовая кнопка приложения
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@Composable
fun AppTextButton(
    modifier: Modifier = Modifier,
    text: String,
    color: Color = secondaryColor,
    fontSize: TextUnit = 20.sp,
    fontWeight: FontWeight = FontWeight.Bold,
    fontFamily: FontFamily = lato,
    onClick: () -> Unit
) {
    Text(
        text = text,
        fontSize = fontSize,
        fontWeight = fontWeight,
        fontFamily = fontFamily,
        color = color,
        modifier = modifier.clickable { onClick() }
    )
}

/*
Описание: Стандартная кнопка приложения
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@Composable
fun AppButton(
    modifier: Modifier = Modifier,
    text: String,
    color: Color = Color.White,
    fontSize: TextUnit = 17.sp,
    fontWeight: FontWeight = FontWeight.SemiBold,
    fontFamily: FontFamily = sourceSans,
    enabled: Boolean = true,
    contentPadding: PaddingValues = PaddingValues(16.dp),
    colors: ButtonColors = ButtonDefaults.buttonColors(backgroundColor = primaryColor, disabledBackgroundColor = primaryDisabledColor),
    borderStroke: BorderStroke = BorderStroke(1.dp, Color.Transparent),
    onClick: () -> Unit
) {
    Button(
        modifier = modifier,
        shape = MaterialTheme.shapes.medium,
        enabled = enabled,
        elevation = ButtonDefaults.elevation(0.dp),
        colors = colors,
        contentPadding = contentPadding,
        border = borderStroke,
        onClick = onClick
    ) {
        Text(
            text = text,
            fontSize = fontSize,
            fontWeight = fontWeight,
            fontFamily = fontFamily,
            color = color
        )
    }
}

/*
Описание: Кнопка перехода на предыдущий экран
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@Composable
fun AppBackButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit
) {
    Card(
        modifier = modifier.clickable { onClick() },
        backgroundColor = inputColor,
        shape = MaterialTheme.shapes.small,
        elevation = 0.dp
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_back),
            contentDescription = "",
            tint = textColor,
            modifier = Modifier.padding(6.dp)
        )
    }
}

/*
Описание: Кнопка закрытия окна
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@Composable
fun AppDismissButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit
) {
    Card(
        modifier = modifier.clickable { onClick() },
        backgroundColor = Color(0xFF818C99).copy(0.12f),
        shape = CircleShape,
        elevation = 0.dp
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_dismiss),
            contentDescription = "",
            tint = textColor,
            modifier = Modifier.padding(7.dp)
        )
    }
}

/*
Описание: Кнопка перехода в корзину
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@Composable
fun AppCartButton(
    modifier: Modifier = Modifier,
    price: String,
    onClick: () -> Unit
) {
    Button(
        modifier = modifier,
        shape = MaterialTheme.shapes.medium,
        elevation = ButtonDefaults.elevation(0.dp),
        colors = ButtonDefaults.buttonColors(backgroundColor = primaryColor, disabledBackgroundColor = primaryDisabledColor),
        contentPadding = PaddingValues(16.dp),
        onClick = onClick
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_cart),
                    contentDescription = "",
                    tint = Color.White
                )
                Spacer(modifier = Modifier.width(16.dp))
                Text(
                    text = "В корзину",
                    fontSize = 17.sp,
                    fontWeight = FontWeight.SemiBold,
                    fontFamily = sourceSans,
                    color = Color.White
                )
            }
            Text(
                text = "$price ₽",
                fontSize = 17.sp,
                fontWeight = FontWeight.SemiBold,
                fontFamily = sourceSans,
                color = Color.White
            )
        }
    }
}