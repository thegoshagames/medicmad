package com.example.medicmad.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.medicmad.view.ui.components.AppTextButton
import com.example.medicmad.view.ui.theme.MedicMADTheme
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.rememberPagerState
import com.example.medicmad.R
import com.example.medicmad.view.ui.components.OnboardComponent
import com.example.medicmad.view.ui.theme.secondaryColor
import com.google.accompanist.pager.HorizontalPager

/*
Описание: Класс приветственных экранов
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
class OnboardActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MedicMADTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ScreenContent()
                }
            }
        }
    }

    /*
    Описание: Контент приветственных экранов
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    @OptIn(ExperimentalPagerApi::class)
    @Composable
    fun ScreenContent() {
        val mContext = LocalContext.current
        val shared = this.getSharedPreferences("shared", Context.MODE_PRIVATE)

        val imageArray = listOf(
            R.drawable.onboard_image_1,
            R.drawable.onboard_image_2,
            R.drawable.onboard_image_3
        )

        var buttonText by rememberSaveable { mutableStateOf("Пропустить") }

        val pagerState = rememberPagerState()
        LaunchedEffect(pagerState) {
            snapshotFlow { pagerState.currentPage }.collect {
                buttonText = if (it != 2) {
                    "Пропустить"
                } else {
                    "Завершить"
                }
            }
        }

        Column(
            verticalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxSize()
        ) {
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxWidth().padding(top = 5.dp, start = 30.dp)
            ) {
                AppTextButton(
                    text = buttonText
                ) {
                    with(shared.edit()) {
                        putBoolean("isFirstEnter", false)
                        apply()
                    }

                    val intent = Intent(mContext, LoginActivity::class.java)
                    startActivity(intent)
                }
                Image(
                    painter = painterResource(id = R.drawable.onboard_logo),
                    contentDescription = ""
                )
            }
            Spacer(modifier = Modifier.height(15.dp))
            HorizontalPager(
                count = 3,
                state = pagerState,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(120.dp)
                    .testTag("pager")
                ,
            ) {
                when (it) {
                    0 -> {
                        OnboardComponent(
                            title = "Анализы",
                            text = "Экспресс сбор и получение проб"
                        )
                    }
                    1 -> {
                        OnboardComponent(
                            title = "Уведомления",
                            text = "Вы быстро узнаете о результатах"
                        )
                    }
                    2 -> {
                        OnboardComponent(
                            title = "Мониторинг",
                            text = "Наши врачи всегда наблюдают за вашими показателями здоровья"
                        )
                    }
                }
            }
            Spacer(modifier = Modifier.height(15.dp))
            Row(
                horizontalArrangement = Arrangement.Center,
                modifier = Modifier.fillMaxWidth()
            ) {
                for (i in 0 until pagerState.pageCount) {
                    Box(
                        modifier = Modifier
                            .padding(4.dp)
                            .size(13.dp)
                            .clip(CircleShape)
                            .background(if (i == pagerState.currentPage) secondaryColor else Color.White)
                            .border(0.64.dp, secondaryColor, CircleShape)
                    )
                }
            }
            Spacer(modifier = Modifier.height(50.dp))
            Image(
                painter = painterResource(imageArray[pagerState.currentPage]),
                contentDescription = "",
                contentScale = ContentScale.FillHeight,
                modifier = Modifier
                    .heightIn(max = 270.dp)
                    .fillMaxSize()
                    .padding(bottom = 56.dp)
            )
        }
    }
}