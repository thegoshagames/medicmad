package com.example.medicmad.view

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.Animatable
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.example.medicmad.R
import com.example.medicmad.view.ui.theme.MedicMADTheme
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/*
Описание: Класс экрана успешной оплаты заказа
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
class SuccessActivity : ComponentActivity() {
    @SuppressLint("CoroutineCreationDuringComposition")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            var scope = rememberCoroutineScope()
            var isLoading by rememberSaveable { mutableStateOf(true) }

            var rotation = remember { androidx.compose.animation.core.Animatable(0f) }
            LaunchedEffect(Unit) {
                rotation.animateTo(
                    animationSpec = infiniteRepeatable(
                        animation = tween(
                            1000,
                            easing = LinearEasing
                        ),
                        repeatMode = RepeatMode.Restart
                    ),
                    targetValue = 360f
                )
            }

            scope.launch {
                delay(1000)
                isLoading = false
            }
            
            MedicMADTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Scaffold(
                        topBar = {
                            Text(
                                text = "Оплата",
                                fontSize = 20.sp,
                                fontWeight = FontWeight.SemiBold,
                                modifier = Modifier.fillMaxWidth(),
                                textAlign = TextAlign.Center
                            )
                        }
                    ) { padding ->
                        Box(modifier = Modifier
                            .padding(padding)
                            .fillMaxSize()) {
                            if (isLoading) {
                                Column(
                                    horizontalAlignment = Alignment.CenterHorizontally,
                                    verticalArrangement = Arrangement.SpaceBetween,
                                    modifier = Modifier.align(Alignment.Center)
                                ) {
                                    Image(
                                        painter = painterResource(id = R.drawable.ic_loading),
                                        contentDescription = "",
                                        modifier = Modifier.rotate(rotation.value)
                                    )
                                }
                            } else {
                                Column() {
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}