package com.example.medicmad.view

import android.app.Application
import android.content.Intent
import android.content.SharedPreferences
import android.provider.ContactsContract.Profile
import android.provider.MediaStore
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.result.ActivityResultLauncher
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.compose.ui.window.Dialog
import com.example.medicmad.R
import com.example.medicmad.common.DataSaver
import com.example.medicmad.common.UserService
import com.example.medicmad.model.UserCard
import com.example.medicmad.view.ui.components.AppButton
import com.example.medicmad.view.ui.components.AppTextButton
import com.example.medicmad.view.ui.components.AppTextField
import com.example.medicmad.view.ui.theme.*
import com.example.medicmad.viewmodel.HomeViewModel
import com.example.medicmad.viewmodel.LoginViewModel
import com.example.medicmad.viewmodel.UserViewModel
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage

/*
Описание: Экран "Профиль"
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ProfileScreen(
    application: Application,
    viewModel: UserViewModel,
    shared: SharedPreferences,
    imageResultLauncher: ActivityResultLauncher<Intent>,
    videoResultLauncher: ActivityResultLauncher<Intent>
) {
    val mContext = LocalContext.current

    val userList = UserService().getUserList(shared)

    var firstName by rememberSaveable { mutableStateOf("") }
    var patronymic by rememberSaveable { mutableStateOf("") }
    var lastName by rememberSaveable { mutableStateOf("") }
    var birthday by rememberSaveable { mutableStateOf("") }
    var gender by rememberSaveable { mutableStateOf("") }
    var image by rememberSaveable { mutableStateOf("") }

    var isLoading by rememberSaveable { mutableStateOf(false) }
    var isAlertVisible by rememberSaveable { mutableStateOf(false) }

    var chooseDialogOpened by rememberSaveable { mutableStateOf(false) }

    var isDropdownOpened by rememberSaveable { mutableStateOf(false) }

    val selectedImage by viewModel.selectedImage.observeAsState()
    val selectedVideo by viewModel.selectedVideo.observeAsState()

    val user by viewModel.user.observeAsState()
    LaunchedEffect(user) {
        isLoading = false

        if (user != null) {
            userList.removeAt(0)
            userList.add(0, user!!)

            UserService().saveUser(shared, userList)

            val intent = Intent(mContext, HomeActivity::class.java)
            mContext.startActivity(intent)
        }
    }

    LaunchedEffect(Unit) {
        if (userList.isEmpty()) {
            val intent = Intent(mContext, CreateCardActivity::class.java)
            mContext.startActivity(intent)
        } else {
            firstName = userList[0].firstname
            patronymic = userList[0].middlename
            lastName = userList[0].lastname
            birthday = userList[0].bith
            gender = userList[0].pol
            image = userList[0].image.toString()
        }
    }

    Scaffold(
        topBar = {
            Text(
                text = "Карта пациента",
                fontSize = 24.sp,
                fontWeight = FontWeight.Bold,
                fontFamily = sourceSans,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 17.dp, bottom = 7.dp)
            )
        }
    ) { padding ->
        Box(modifier = Modifier
            .padding(padding)
            .fillMaxSize()) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp)
                    .verticalScroll(rememberScrollState())
            ) {
                Column(
                    modifier = Modifier
                        .widthIn(max = 400.dp)
                        .fillMaxWidth()
                ) {
                    Box(
                        modifier = Modifier
                            .size(148.dp, 123.dp)
                            .clip(RoundedCornerShape(100))
                            .background(Color(0xFFD9D9D9).copy(0.5f))
                            .align(Alignment.CenterHorizontally)
                            .clickable {
                                chooseDialogOpened = true
                            }
                    ) {
                        if ((image == "" || image == "null") && selectedImage == null && selectedVideo == null) {
                            Image(
                                painter = painterResource(id = R.drawable.image_default),
                                contentDescription = "",
                                contentScale = ContentScale.Crop,
                                modifier = Modifier.align(Alignment.Center)
                            )
                        } else {
                            if (selectedVideo != null) {
                                val exoPlayer = ExoPlayer.Builder(mContext).build().apply {
                                    val dataSourceFactory = DefaultDataSourceFactory(mContext, mContext.packageName)
                                    val source = ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(MediaItem.fromUri(selectedVideo.toString()))

                                    prepare(source)
                                }

                                AndroidView(
                                    modifier = Modifier.align(Alignment.Center),
                                    factory = {
                                    PlayerView(it).apply {
                                        player = exoPlayer
                                        exoPlayer.repeatMode = Player.REPEAT_MODE_ALL
                                        useController = false

                                        exoPlayer.play()
                                    }
                                })
                            } else {
                                GlideImage(
                                    imageModel = if (selectedImage == null) image else selectedImage,
                                    imageOptions = ImageOptions(contentScale = ContentScale.Crop, alignment = Alignment.Center)
                                )
                            }
                        }
                    }
                    Spacer(modifier = Modifier.height(16.dp))
                    Text(
                        text = "Без карты пациента вы не сможете заказать анализы.",
                        fontSize = 14.sp,
                        fontFamily = sourceSans,
                        color = descriptionColor
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    Text(
                        text = "В картах пациентов будут храниться результаты анализов вас и ваших близких.",
                        fontSize = 14.sp,
                        fontFamily = sourceSans,
                        color = descriptionColor
                    )
                    Spacer(modifier = Modifier.height(32.dp))
                    AppTextField(
                        value = firstName,
                        onValueChange = {
                            firstName = it
                        },
                        modifier = Modifier.fillMaxWidth(),
                        placeholder = {
                            Text(
                                text = "Имя",
                                fontSize = 15.sp,
                                fontFamily = sourceSans,
                                color = descriptionColor
                            )
                        },
                        stroke = if (firstName.isNotEmpty()) iconColor else strokeColor
                    )
                    Spacer(modifier = Modifier.height(24.dp))
                    AppTextField(
                        value = patronymic,
                        onValueChange = {
                            patronymic = it
                        },
                        modifier = Modifier.fillMaxWidth(),
                        placeholder = {
                            Text(
                                text = "Отчество",
                                fontSize = 15.sp,
                                fontFamily = sourceSans,
                                color = descriptionColor
                            )
                        },
                        stroke = if (patronymic.isNotEmpty()) iconColor else strokeColor
                    )
                    Spacer(modifier = Modifier.height(24.dp))
                    AppTextField(
                        value = lastName,
                        onValueChange = {
                            lastName = it
                        },
                        modifier = Modifier.fillMaxWidth(),
                        placeholder = {
                            Text(
                                text = "Фамилия",
                                fontSize = 15.sp,
                                fontFamily = sourceSans,
                                color = descriptionColor
                            )
                        },
                        stroke = if (lastName.isNotEmpty()) iconColor else strokeColor
                    )
                    Spacer(modifier = Modifier.height(24.dp))
                    AppTextField(
                        value = birthday,
                        onValueChange = {
                            birthday = it
                        },
                        modifier = Modifier.fillMaxWidth(),
                        placeholder = {
                            Text(
                                text = "Дата рождения",
                                fontSize = 15.sp,
                                fontFamily = sourceSans,
                                color = descriptionColor
                            )
                        },
                        stroke = if (birthday.isNotEmpty()) iconColor else strokeColor
                    )
                    Spacer(modifier = Modifier.height(24.dp))
                    ExposedDropdownMenuBox(expanded = isDropdownOpened, onExpandedChange = { isDropdownOpened = !isDropdownOpened }) {
                        AppTextField(
                            value = gender,
                            onValueChange = {
                                gender = it
                            },
                            modifier = Modifier.fillMaxWidth(),
                            placeholder = {
                                Text(
                                    text = "Пол",
                                    fontSize = 15.sp,
                                    fontFamily = sourceSans,
                                    color = descriptionColor
                                )
                            },
                            trailingIcon = {
                                Icon(
                                    painter = painterResource(id = R.drawable.ic_drop),
                                    contentDescription = "",
                                    tint = textColor,
                                    modifier = Modifier.clickable {

                                    }
                                )
                            }
                        )
                        DropdownMenu(expanded = isDropdownOpened, onDismissRequest = { isDropdownOpened = !isDropdownOpened }) {
                            DropdownMenuItem(onClick = {
                                gender = "Мужской"
                                isDropdownOpened = false
                            }) {
                                Text(text = "Мужской")
                            }
                            DropdownMenuItem(onClick = {
                                gender = "Женский"
                                isDropdownOpened = false
                            }) {
                                Text(text = "Женский")
                            }
                        }
                    }
                    Spacer(modifier = Modifier.height(48.dp))
                    AppButton(
                        text = "Сохранить",
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        isLoading = true

                        viewModel.updateCard(
                            DataSaver(application).getStringFromSharedPreferences("token"),
                            userCard = UserCard(
                                firstname = firstName,
                                middlename = patronymic,
                                lastname = lastName,
                                bith = birthday,
                                pol = gender
                            )
                        )
                    }
                }
            }
        }
    }

    if (chooseDialogOpened) {
            AlertDialog(
                onDismissRequest = {
                    chooseDialogOpened = false
                    viewModel.errorMessage.value = null
                },
                title = { Text("Выбор") },
                text = { Text("") },
                buttons = {
                    Row(
                        horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        TextButton(onClick = {
                            chooseDialogOpened = false

                            val intent = Intent()
                            intent.action = MediaStore.ACTION_IMAGE_CAPTURE

                            imageResultLauncher.launch(intent)
                        }) {
                            Text(text = "Фото")
                        }

                        TextButton(onClick = {
                            chooseDialogOpened = false

                            val intent = Intent()
                            intent.action = MediaStore.ACTION_VIDEO_CAPTURE
                            //intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 3)

                            videoResultLauncher.launch(intent)
                        }) {
                            Text(text = "Видео")

                        }
                    }
                }
            )
    }

    if (isLoading) {
        Dialog(onDismissRequest = {}) {
            CircularProgressIndicator()
        }
    }

    if (isAlertVisible) {
        AlertDialog(
            onDismissRequest = {
                isAlertVisible = false
                viewModel.errorMessage.value = null
            },
            title = { Text("Ошибка") },
            text = { Text(viewModel.errorMessage.value.toString()) },
            buttons = {
                TextButton(onClick = {
                    isAlertVisible = false
                    viewModel.errorMessage.value = null
                }) {
                    Text(text = "OK")
                }
            }
        )
    }
}