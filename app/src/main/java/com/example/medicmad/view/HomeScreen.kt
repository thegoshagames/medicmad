package com.example.medicmad.view

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import android.widget.Space
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.vector.DefaultStrokeLineMiter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import com.example.medicmad.R
import com.example.medicmad.common.CartService
import com.example.medicmad.common.UserService
import com.example.medicmad.model.CartItem
import com.example.medicmad.model.CatalogItem
import com.example.medicmad.view.ui.components.*
import com.example.medicmad.view.ui.theme.descriptionColor
import com.example.medicmad.view.ui.theme.primaryColor
import com.example.medicmad.view.ui.theme.sourceSans
import com.example.medicmad.view.ui.theme.textColor
import com.example.medicmad.viewmodel.HomeViewModel
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.Locale.Category


/*
Описание: Экран "Анализы"
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreen(
    viewModel: HomeViewModel,
    shared: SharedPreferences
) {
    val mContext = LocalContext.current

    val modalBottomSheetState = rememberModalBottomSheetState(initialValue = ModalBottomSheetValue.Hidden, skipHalfExpanded = true)

    val categoryList = listOf(
        "Популярные",
        "Covid",
        "Комплексные",
        "Чекапы",
        "Биохимия",
        "Гормоны",
        "Иммунитет",
        "Витамины",
        "Аллергены",
        "Анализ крови",
        "Анализ мочи",
        "Анализ кала",
        "Только в клинике"
    )
    
    var selectedCategory by rememberSaveable { mutableStateOf("Популярные") }

    var isSearchEnabled by rememberSaveable { mutableStateOf(false) }

    val searchInteractionSource = remember { MutableInteractionSource() }
    if (searchInteractionSource.collectIsPressedAsState().value) {
        isSearchEnabled = true
    }

    var selectedCatalogItem: CatalogItem? by remember { mutableStateOf(null) }

    var cart: MutableList<CartItem> = remember { mutableStateListOf()}

    var searchText by rememberSaveable { mutableStateOf("") }

    var isVisible by rememberSaveable { mutableStateOf(true) }
    
    var isLoading by rememberSaveable { mutableStateOf(false) }
    var isAlertVisible by rememberSaveable { mutableStateOf(false) }

    val responseCode by viewModel.responseCode.observeAsState()
    LaunchedEffect(responseCode) {
        isLoading = false
    }
    
    val errorMessage by viewModel.errorMessage.observeAsState()
    LaunchedEffect(errorMessage) {
        isLoading = false

        if (errorMessage != null) {
            isAlertVisible = true
        }
    }

    LaunchedEffect(Unit) {
        isLoading = true

        viewModel.getNews()
        viewModel.getCatalog()

        cart.addAll(CartService().getCart(shared))
    }

    var lazyListState = rememberLazyListState()
    LaunchedEffect(lazyListState) {
        snapshotFlow { lazyListState.firstVisibleItemScrollOffset }.collect {
            isVisible = it <= 0
        }
    }

    val swipeRefreshState = rememberSwipeRefreshState(isRefreshing = isLoading)

    val scope = rememberCoroutineScope()

    ModalBottomSheetLayout(
        sheetState = modalBottomSheetState,
        sheetShape = RoundedCornerShape(topStart = 24.dp, topEnd = 24.dp),
        sheetContent = {
            if (selectedCatalogItem != null) {
                AnalysisBottomSheet(
                    catalogItem = selectedCatalogItem!!,
                    onDismiss = {
                        scope.launch {
                            modalBottomSheetState.hide()
                        }
                    }
                ) {

                }
            } else {
                Text(text = "Товар")
            }
        }
    ) {
        SwipeRefresh(
            state = swipeRefreshState,
            onRefresh = {
                isLoading = true

                viewModel.getNews()
                viewModel.getCatalog()
            }
        ) {
            Scaffold(
                topBar = {
                    Row(
                        horizontalArrangement = Arrangement.Center,
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier
                            .fillMaxWidth()
                            .drawBehind {
                                if (isSearchEnabled) {
                                    drawLine(
                                        descriptionColor.copy(0.2f),
                                        Offset(0f, size.height),
                                        Offset(size.width, size.height),
                                        Stroke.DefaultMiter
                                    )
                                }
                            }
                            .padding(top = 24.dp, bottom = if (isSearchEnabled) 16.dp else 32.dp)
                            .padding(horizontal = 20.dp)
                    ) {
                        AppTextField(
                            modifier = Modifier
                                .fillMaxWidth(if (isSearchEnabled) 0.75f else 1f),
                            value = searchText,
                            onValueChange = {
                                searchText = it
                            },
                            leadingIcon = {
                                Icon(
                                    painter = painterResource(id = R.drawable.ic_search),
                                    contentDescription = "",
                                    tint = textColor
                                )
                            },
                            trailingIcon = {
                                if (isSearchEnabled) {
                                    Icon(
                                        painter = painterResource(id = R.drawable.ic_dismiss),
                                        contentDescription = "",
                                        tint = textColor,
                                        modifier = Modifier.clickable {
                                            searchText = ""
                                        }
                                    )
                                }
                            },
                            placeholder = {
                                Text(
                                    text = if (!isSearchEnabled) "Искать анализы" else "",
                                    fontSize = 16.sp,
                                    fontFamily = sourceSans,
                                    color = descriptionColor
                                )
                            },
                            interactionSource = searchInteractionSource
                        )
                        AnimatedVisibility(visible = isSearchEnabled) {
                            AppTextButton(
                                text = "Отменить",
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Normal,
                                fontFamily = sourceSans,
                                color = primaryColor
                            ) {
                                isSearchEnabled = false
                            }
                        }
                    }
                }
            ) { padding ->
                Box(modifier = Modifier.padding(padding)) {
                    AnimatedVisibility(visible = !isSearchEnabled) {
                        Column(modifier = Modifier.fillMaxWidth()) {
                            Column(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(start = 20.dp)
                                    .verticalScroll(rememberScrollState())
                            ) {
                                AnimatedVisibility(visible = isVisible && cart.isEmpty()) {
                                    Column(
                                        modifier = Modifier.fillMaxWidth()
                                    ) {
                                        Text(
                                            text = "Акции и новости",
                                            fontSize = 17.sp,
                                            fontWeight = FontWeight.SemiBold,
                                            fontFamily = sourceSans,
                                            color = descriptionColor
                                        )
                                        Spacer(modifier = Modifier.height(16.dp))
                                        LazyRow {
                                            items(viewModel.news.distinct()) {
                                                NewsComponent(
                                                    title = it.name,
                                                    text = it.description,
                                                    cost = it.price,
                                                    image = it.image
                                                )
                                                Spacer(modifier = Modifier.width(16.dp))
                                            }
                                        }
                                        Spacer(modifier = Modifier.height(32.dp))
                                        Text(
                                            text = "Каталог анализов",
                                            fontSize = 17.sp,
                                            fontWeight = FontWeight.SemiBold,
                                            fontFamily = sourceSans,
                                            color = descriptionColor
                                        )
                                        Spacer(modifier = Modifier.height(16.dp))
                                    }
                                }
                            }
                            LazyRow(
                                Modifier
                                    .fillMaxWidth()
                                    .padding(start = 20.dp)
                            ) {
                                items(categoryList) {
                                    CategoryComponent(
                                        title = it,
                                        isSelected = selectedCategory.lowercase() == it.lowercase()
                                    ) {
                                        selectedCategory = it
                                    }
                                    Spacer(modifier = Modifier.width(16.dp))
                                }
                            }
                            LazyColumn(
                                state = lazyListState,
                                modifier = Modifier.padding(horizontal = 20.dp)
                            ) {
                                item {
                                    Spacer(modifier = Modifier.height(24.dp))
                                }
                                items(viewModel.catalog.filter { it.category.lowercase() == selectedCategory.lowercase() }.distinct()) { item ->
                                    CatalogBlock(
                                        item,
                                        itemInCart = cart.any { it.id == item.id },
                                        onCardClick = {
                                            scope.launch {
                                                selectedCatalogItem = item

                                                modalBottomSheetState.show()
                                            }
                                        },
                                        onClick = {
                                            val itemIndex = cart.indexOfFirst { it.id == item.id }

                                            if (itemIndex != -1) {
                                                cart.removeAt(itemIndex)

                                                CartService().saveCart(shared, cart)
                                            } else {
                                                cart.add(
                                                    CartItem(
                                                        item.id,
                                                        item.name,
                                                        item.price,
                                                        1
                                                    )
                                                )

                                                CartService().saveCart(shared, cart)
                                            }
                                        }
                                    )
                                    Spacer(modifier = Modifier.height(16.dp))
                                }
                                item {
                                    Spacer(modifier = Modifier.height(100.dp))
                                }
                            }
                        }
                    }
                    AnimatedVisibility(visible = isSearchEnabled) {
                        LazyColumn(modifier = Modifier.fillMaxWidth()) {
                            items(viewModel.catalog.filter { (it.name.lowercase().contains(searchText.lowercase()) || it.bio.lowercase().contains(searchText.lowercase())) && searchText.isNotEmpty() }.distinct()) { item ->
                                SearchComponent(
                                    catalogItem = item,
                                    searchText = searchText
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    Box(modifier = Modifier
        .fillMaxSize()
    ) {
        var cartPrice = 0

        for (cartItem in cart.distinct()) {
            cartPrice += cartItem.price.toInt() * cartItem.count
        }

        if (cartPrice > 0) {
            Box(modifier = Modifier
                .drawBehind {
                    drawLine(
                        descriptionColor.copy(0.2f),
                        Offset(0f, 0f),
                        Offset(size.width, 0f),
                        Stroke.DefaultMiter
                    )
                }
                .background(Color.White)
                .fillMaxWidth()
                .padding(horizontal = 20.dp, vertical = 24.dp)
                .align(Alignment.BottomCenter)
            ) {
                AppCartButton(
                    price = cartPrice.toString()
                ) {
                    val intent = Intent(mContext, CartActivity::class.java)
                    mContext.startActivity(intent)
                }
            }
        }
    }

    if (isLoading) {
        Dialog(onDismissRequest = {}) {
            CircularProgressIndicator()
        }
    }

    if (isAlertVisible) {
        AlertDialog(
            onDismissRequest = {
                isAlertVisible = false
                viewModel.errorMessage.value = null
            },
            title = { Text("Ошибка") },
            text = { Text(viewModel.errorMessage.value.toString()) },
            buttons = {
                TextButton(onClick = {
                    isAlertVisible = false
                    viewModel.errorMessage.value = null
                }) {
                    Text(text = "OK")
                }
            }
        )
    }
}