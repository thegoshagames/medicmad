package com.example.medicmad.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import com.example.medicmad.R
import com.example.medicmad.viewmodel.HomeViewModel

/*
Описание: Экран "Результаты"
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@Composable
fun ResultsScreen() {
    Box(modifier = Modifier.fillMaxSize()) {
        Image(painter = painterResource(
            id = R.drawable.ic_medic_foreground),
            contentDescription = "",
            modifier = Modifier.align(Alignment.Center)
        )
    }
}