package com.example.medicmad.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.registerForActivityResult
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.vector.DefaultStrokeLineMiter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavHostController
import androidx.navigation.Navigation
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.medicmad.viewmodel.HomeViewModel
import com.example.medicmad.R
import com.example.medicmad.view.ui.theme.*
import com.example.medicmad.viewmodel.UserViewModel

/*
Описание: Класс главных экранов приложения
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
class HomeActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()
            val navBackStackEntry by navController.currentBackStackEntryAsState()

            MedicMADTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Scaffold(
                        bottomBar = {
                            BottomNavigation(
                                backgroundColor = Color.White,
                                elevation = 0.dp,
                                modifier = Modifier.drawBehind {
                                    drawLine(
                                        descriptionColor.copy(0.2f),
                                        Offset(0f, 0f),
                                        Offset(size.width, 0f),
                                        Stroke.DefaultMiter
                                    )
                                }
                            ) {
                                BottomNavigationItem(
                                    selected = navBackStackEntry?.destination?.route == "home",
                                    icon = {
                                           Icon(
                                               painter = painterResource(id = R.drawable.analysis),
                                               contentDescription = ""
                                           )
                                    },
                                    label = {
                                        Text(
                                            text = "Анализы",
                                            fontSize = 12.sp,
                                            fontWeight = FontWeight.Normal,
                                            fontFamily = sourceSans
                                        )
                                    },
                                    selectedContentColor = primaryColor,
                                    unselectedContentColor = iconColor,
                                    onClick = { navController.navigate("home") }
                                )
                                BottomNavigationItem(
                                    selected = navBackStackEntry?.destination?.route == "results",
                                    icon = {
                                        Icon(
                                            painter = painterResource(id = R.drawable.results),
                                            contentDescription = ""
                                        )
                                    },
                                    label = {
                                        Text(
                                            text = "Результаты",
                                            fontSize = 12.sp,
                                            fontWeight = FontWeight.Normal,
                                            fontFamily = sourceSans
                                        )
                                    },
                                    selectedContentColor = primaryColor,
                                    unselectedContentColor = iconColor,
                                    onClick = { navController.navigate("results") }
                                )
                                BottomNavigationItem(
                                    selected = navBackStackEntry?.destination?.route == "support",
                                    icon = {
                                        Icon(
                                            painter = painterResource(id = R.drawable.analysis),
                                            contentDescription = ""
                                        )
                                    },
                                    label = {
                                        Text(
                                            text = "Поддержка",
                                            fontSize = 12.sp,
                                            fontWeight = FontWeight.Normal,
                                            fontFamily = sourceSans
                                        )
                                    },
                                    selectedContentColor = primaryColor,
                                    unselectedContentColor = iconColor,
                                    onClick = { navController.navigate("support") }
                                )
                                BottomNavigationItem(
                                    selected = navBackStackEntry?.destination?.route == "profile",
                                    icon = {
                                        Icon(
                                            painter = painterResource(id = R.drawable.profile),
                                            contentDescription = ""
                                        )
                                    },
                                    selectedContentColor = primaryColor,
                                    unselectedContentColor = iconColor,
                                    label = {
                                        Text(
                                            text = "Профиль",
                                            fontSize = 12.sp,
                                            fontWeight = FontWeight.Normal,
                                            fontFamily = sourceSans
                                        )
                                    },
                                    onClick = { navController.navigate("profile") }
                                )
                            }
                        }
                    ) { padding ->
                        Box(modifier = Modifier.padding(padding)) {
                            NavHost(navHostController = navController)
                        }
                    }
                }
            }
        }
    }

    /*
    Описание: Функция навигации главных экранов
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    @Composable
    fun NavHost(
        navHostController: NavHostController
    ) {
        val homeViewModel = ViewModelProvider(this)[HomeViewModel::class.java]
        val userViewModel = ViewModelProvider(this)[UserViewModel::class.java]
        val sharedPreferences = this.getSharedPreferences("shared", Context.MODE_PRIVATE)

        NavHost(
            navController = navHostController,
            startDestination = "home"
        ) {
            composable("home") {
                HomeScreen(
                    homeViewModel,
                    sharedPreferences
                )
            }
            composable("results") {
                ResultsScreen()
            }
            composable("support") {
                SupportScreen()
            }
            composable("profile") {
                ProfileScreen(
                    application,
                    userViewModel,
                    sharedPreferences,
                    imageResultLauncher, videoResultLauncher
                )
            }
        }
    }

    /*
    Описание: Переход на Activity камеры (фото)
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    val imageResultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val userViewModel = ViewModelProvider(this)[UserViewModel::class.java]

            val data: Intent? = result.data

            userViewModel.selectedImage.value = data!!.extras!!.get("data") as Bitmap
            userViewModel.selectedVideo.value = null
        }
    }

    /*
    Описание: Переход на Activity камеры (видео)
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    val videoResultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val userViewModel = ViewModelProvider(this)[UserViewModel::class.java]

            val data: Intent? = result.data

            userViewModel.selectedVideo.value = data!!.data.toString()
            userViewModel.selectedImage.value = null
        }
    }
}