package com.example.medicmad.view.ui.components

import android.app.DatePickerDialog
import android.content.Intent
import android.provider.Telephony.Mms.Addr
import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.medicmad.model.CatalogItem
import com.example.medicmad.R
import com.example.medicmad.model.AddressItem
import com.example.medicmad.model.UserCard
import com.example.medicmad.view.CreateCardActivity
import com.example.medicmad.view.ui.theme.*
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import java.util.Calendar

/*
Описание: Окно "Карточка товара"
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@Composable
fun AnalysisBottomSheet(
    catalogItem: CatalogItem,
    onDismiss: () -> Unit,
    onButtonClick: () -> Unit
) {
    Column(modifier = Modifier
        .fillMaxWidth()
        .padding(horizontal = 20.dp)
        .padding(top = 24.dp)
        .verticalScroll(rememberScrollState())
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = catalogItem.name,
                fontSize = 20.sp,
                fontWeight = FontWeight.SemiBold,
                fontFamily = sourceSans,
                modifier = Modifier.fillMaxWidth(0.7f)
            )
            AppDismissButton(onClick = onDismiss)
        }
        Spacer(modifier = Modifier.height(20.dp))
        Text(
            text = "Описание",
            fontSize = 16.sp,
            fontWeight = FontWeight.Normal,
            fontFamily = sourceSans,
            color = descriptionColor
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            text = catalogItem.description,
            fontSize = 15.sp,
            fontWeight = FontWeight.Normal,
            fontFamily = sourceSans
        )
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            text = "Подготовка",
            fontSize = 16.sp,
            fontWeight = FontWeight.Normal,
            fontFamily = sourceSans,
            color = descriptionColor
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            text = catalogItem.preparation,
            fontSize = 15.sp,
            fontWeight = FontWeight.Normal,
            fontFamily = sourceSans
        )
        Spacer(modifier = Modifier.height(57.dp))
        Row(Modifier.fillMaxWidth()) {
            Column(modifier = Modifier.weight(1f)) {
                Text(
                    text = "Результаты через:",
                    fontSize = 14.sp,
                    fontFamily = sourceSans,
                    fontWeight = FontWeight.SemiBold,
                    color = descriptionColor
                )
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    text = catalogItem.time_result,
                    fontSize = 16.sp,
                    fontFamily = sourceSans,
                    fontWeight = FontWeight.Normal
                )
            }
            Column(modifier = Modifier.weight(1f)) {
                Text(
                    text = "Биоматериал:",
                    fontSize = 14.sp,
                    fontFamily = sourceSans,
                    fontWeight = FontWeight.SemiBold,
                    color = descriptionColor
                )
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    text = catalogItem.bio,
                    fontSize = 16.sp,
                    fontFamily = sourceSans,
                    fontWeight = FontWeight.Normal
                )
            }
        }
        Spacer(modifier = Modifier.height(19.dp))
        AppButton(
            text = "Добавить за ${catalogItem.price} ₽",
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 40.dp),
            onClick = onButtonClick
        )
    }
}

/*
Описание: Окно выбора адреса заказа
Дата создания: 10.04.2023
Автор: Георгий Хасанов
 */
@Composable
fun AddressBottomSheet(
    address: String,
    lat: String,
    lng: String,
    alt: String,
    onAddressSave: (AddressItem) -> Unit,
    onAddressConfirm: (AddressItem) -> Unit,
) {
    var addressText by rememberSaveable { mutableStateOf(address) }

    var latitude by rememberSaveable { mutableStateOf(lat) }
    var longitude by rememberSaveable { mutableStateOf(lng) }
    var altitude by rememberSaveable { mutableStateOf(alt) }

    var flat by rememberSaveable { mutableStateOf("") }
    var enter by rememberSaveable { mutableStateOf("") }
    var floor by rememberSaveable { mutableStateOf("") }

    var phoneCodeText by rememberSaveable { mutableStateOf("") }

    var addressName by rememberSaveable { mutableStateOf("") }

    var addressSave by rememberSaveable { mutableStateOf(false) }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp)
            .padding(top = 24.dp, bottom = 32.dp)
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = "Адрес сдачи анализов",
                fontSize = 20.sp,
                fontWeight = FontWeight.SemiBold,
                fontFamily = sourceSans
            )
            Icon(
                painter = painterResource(id = R.drawable.ic_map),
                contentDescription = "",
                tint = iconColor
            )
        }
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            text = "Ваш адрес",
            fontSize = 14.sp,
            fontFamily = sourceSans,
            color = textColor
        )
        Spacer(modifier = Modifier.height(4.dp))
        AppTextField(
            value = addressText,
            onValueChange = {
                 addressText = it
            },
            placeholder = {
                Text(
                    text = "Введите ваш адрес",
                    fontSize = 14.sp,
                    fontFamily = sourceSans,
                    fontWeight = FontWeight.Normal
                )
            },
            modifier = Modifier.fillMaxWidth(),
        )
        Spacer(modifier = Modifier.height(16.dp))
        Row(modifier = Modifier.fillMaxWidth()) {
            Column(modifier = Modifier.weight(4f)) {
                Text(
                    text = "Долгота",
                    fontSize = 14.sp,
                    fontFamily = sourceSans,
                    color = textColor
                )
                Spacer(modifier = Modifier.height(4.dp))
                AppTextField(
                    value = longitude,
                    onValueChange = {},
                    readOnly = true,
                    placeholder = {
                        Text(
                            text = "",
                            fontSize = 14.sp,
                            fontFamily = sourceSans,
                            fontWeight = FontWeight.Normal
                        )
                    },
                    modifier = Modifier.fillMaxWidth()
                )
            }
            Spacer(modifier = Modifier.width(12.5.dp))
            Column(modifier = Modifier.weight(4f)) {
                Text(
                    text = "Широта",
                    fontSize = 14.sp,
                    fontFamily = sourceSans,
                    color = textColor
                )
                Spacer(modifier = Modifier.height(4.dp))
                AppTextField(
                    value = latitude,
                    onValueChange = {},
                    readOnly = true,
                    placeholder = {
                        Text(
                            text = "",
                            fontSize = 14.sp,
                            fontFamily = sourceSans,
                            fontWeight = FontWeight.Normal
                        )
                    },
                    modifier = Modifier.fillMaxWidth()
                )
            }
            Spacer(modifier = Modifier.width(12.5.dp))
            Column(modifier = Modifier.weight(2f)) {
                Text(
                    text = "Высота",
                    fontSize = 14.sp,
                    fontFamily = sourceSans,
                    color = textColor
                )
                Spacer(modifier = Modifier.height(4.dp))
                AppTextField(
                    value = altitude,
                    onValueChange = {},
                    readOnly = true,
                    placeholder = {
                        Text(
                            text = "",
                            fontSize = 14.sp,
                            fontFamily = sourceSans,
                            fontWeight = FontWeight.Normal
                        )
                    },
                    modifier = Modifier.fillMaxWidth()
                )
            }
        }
        Spacer(modifier = Modifier.height(16.dp))
        Row(modifier = Modifier.fillMaxWidth()) {
            Column(modifier = Modifier.weight(2f)) {
                Text(
                    text = "Квартира",
                    fontSize = 14.sp,
                    fontFamily = sourceSans,
                    color = textColor
                )
                Spacer(modifier = Modifier.height(4.dp))
                AppTextField(
                    value = flat,
                    onValueChange = {
                        flat = it
                    },
                    placeholder = {
                        Text(
                            text = "",
                            fontSize = 14.sp,
                            fontFamily = sourceSans,
                            fontWeight = FontWeight.Normal
                        )
                    },
                    modifier = Modifier.fillMaxWidth()
                )
            }
            Spacer(modifier = Modifier.width(12.5.dp))
            Column(modifier = Modifier.weight(2f)) {
                Text(
                    text = "Подъезд",
                    fontSize = 14.sp,
                    fontFamily = sourceSans,
                    color = textColor
                )
                Spacer(modifier = Modifier.height(4.dp))
                AppTextField(
                    value = enter,
                    onValueChange = {
                        enter = it
                    },
                    placeholder = {
                        Text(
                            text = "",
                            fontSize = 14.sp,
                            fontFamily = sourceSans,
                            fontWeight = FontWeight.Normal
                        )
                    },
                    modifier = Modifier.fillMaxWidth()
                )
            }
            Spacer(modifier = Modifier.width(12.5.dp))
            Column(modifier = Modifier.weight(2f)) {
                Text(
                    text = "Этаж",
                    fontSize = 14.sp,
                    fontFamily = sourceSans,
                    color = textColor
                )
                Spacer(modifier = Modifier.height(4.dp))
                AppTextField(
                    value = floor,
                    onValueChange = {
                        floor = it
                    },
                    placeholder = {
                        Text(
                            text = "",
                            fontSize = 14.sp,
                            fontFamily = sourceSans,
                            fontWeight = FontWeight.Normal
                        )
                    },
                    modifier = Modifier.fillMaxWidth()
                )
            }
        }
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            text = "Домофон",
            fontSize = 14.sp,
            fontFamily = sourceSans,
            color = textColor
        )
        Spacer(modifier = Modifier.height(4.dp))
        AppTextField(
            value = phoneCodeText,
            onValueChange = {
                addressText = it
            },
            placeholder = {
                Text(
                    text = "",
                    fontSize = 14.sp,
                    fontFamily = sourceSans,
                    fontWeight = FontWeight.Normal
                )
            },
            modifier = Modifier.fillMaxWidth(),
        )
        Spacer(modifier = Modifier.height(10.dp))
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = "Сохранить этот адрес?",
                fontSize = 16.sp,
                fontWeight = FontWeight.Normal,
                fontFamily = sourceSans
            )
            Switch(
                checked = addressSave,
                onCheckedChange = {
                    addressSave = it
                },
                colors = SwitchDefaults.colors(
                    checkedThumbColor = Color.White,
                    checkedTrackColor = primaryColor,
                    checkedTrackAlpha = 1f,
                    uncheckedTrackColor = inputColor,
                    uncheckedTrackAlpha = 1f,
                    uncheckedThumbColor = Color.White
                )
            )
        }
        if (addressSave) {
            Spacer(modifier = Modifier.height(6.dp))
            AppTextField(
                value = addressName,
                onValueChange = {
                    addressName = it
                },
                placeholder = {
                    Text(
                        text = "Название: например дом, работа",
                        fontSize = 15.sp,
                        fontFamily = sourceSans,
                        fontWeight = FontWeight.Normal,
                        color = descriptionColor
                    )
                },
                modifier = Modifier.fillMaxWidth(),
            )
        }
        Spacer(modifier = Modifier.height(18.dp))
        AppButton(
            text = "Подтвердить",
            modifier = Modifier.fillMaxWidth()
        ) {
            val addressItem = AddressItem(
                addressText,
                latitude,
                longitude,
                altitude,
                flat, enter, floor,
                phoneCodeText,
                addressName
            )

            if (addressSave) {
                onAddressSave(addressItem)
            }

            onAddressConfirm(addressItem)
        }
    }
}

/*
Описание: Окно выбора времени заказа
Дата создания: 10.04.2023
Автор: Георгий Хасанов
 */
@Composable
fun TimeBottomSheet(
    onTimeConfirm: (String, LocalDateTime) -> Unit,
    onBackPressed: () -> Unit
) {
    val mContext = LocalContext.current
    var dateText by rememberSaveable { mutableStateOf("") }

    var selectedTime by rememberSaveable { mutableStateOf("10:00") }

    val timeArray = listOf(
        "10:00",
        "13:00",
        "14:00",
        "15:00",
        "16:00",
        "18:00",
        "19:00"
    )

    val calendar = Calendar.getInstance()

    val mYear = Calendar.YEAR
    val mMonth = Calendar.MONTH
    val mDay = Calendar.DAY_OF_MONTH

    val datePickerDialog = DatePickerDialog(
        mContext, { _, year: Int, month: Int, day: Int ->
            dateText = ""
            var date = " $day $month"

            when (month) {
                0 -> { date = date.replace(" 0", " января") }
                1 -> { date = date.replace(" 1", " февраля") }
                2 -> { date = date.replace(" 2", " марта") }
                3 -> { date = date.replace(" 3", " апреля") }
                4 -> { date = date.replace(" 4", " мая") }
                5 -> { date = date.replace(" 5", " июня") }
                6 -> { date = date.replace(" 6", " июля") }
                7 -> { date = date.replace(" 7", " августа") }
                8 -> { date = date.replace(" 8", " сентября") }
                9 -> { date = date.replace(" 9", " октября") }
                10 -> { date = date.replace(" 10", " ноября") }
                11 -> { date = date.replace(" 11", " декабря") }
            }

            if (day == LocalDateTime.now().dayOfMonth && month + 1 == LocalDateTime.now().month.value) {
                dateText += "Сегодня, "
            }

            dateText += date

        }, mYear, mMonth, mDay
    )

    val timeInteraction = remember { MutableInteractionSource() }
    if (timeInteraction.collectIsPressedAsState().value) {
        datePickerDialog.show()
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp)
            .padding(top = 24.dp, bottom = 32.dp)
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = "Дата и время",
                fontSize = 20.sp,
                fontWeight = FontWeight.SemiBold,
                fontFamily = sourceSans
            )
            AppDismissButton {
                onBackPressed()
            }
        }
        Spacer(modifier = Modifier.height(24.dp))
        Text(
            text = "Выберите дату",
            fontSize = 16.sp,
            fontWeight = FontWeight.Normal,
            fontFamily = sourceSans,
            color = descriptionColor
        )
        Spacer(modifier = Modifier.height(16.dp))
        AppTextField(
            value = dateText,
            onValueChange = {},
            readOnly = true,
            placeholder = {
                Text(
                    text = "",
                    fontSize = 14.sp,
                    fontFamily = sourceSans,
                    fontWeight = FontWeight.Normal,
                    color = descriptionColor
                )
            },
            trailingIcon = {
                Icon(
                    painter = painterResource(id = R.drawable.ic_drop),
                    contentDescription = "",
                    tint = textColor
                )
            },
            modifier = Modifier.fillMaxWidth(),
            interactionSource = timeInteraction
        )
        Spacer(modifier = Modifier.height(32.dp))
        Text(
            text = "Выберите время",
            fontSize = 16.sp,
            fontWeight = FontWeight.Normal,
            fontFamily = sourceSans,
            color = descriptionColor
        )
        Spacer(modifier = Modifier.height(16.dp))
        LazyVerticalGrid(
            columns = GridCells.Adaptive(69.dp),
            horizontalArrangement = Arrangement.spacedBy(16.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            items(timeArray) { item ->
                Box(
                    modifier = Modifier
                        .size(69.dp, 40.dp)
                        .clip(MaterialTheme.shapes.medium)
                        .background(if (selectedTime == item) primaryColor else inputColor)
                        .clickable {
                            selectedTime = item
                        }
                ) {
                    Text(
                        text = item,
                        fontSize = 16.sp,
                        fontFamily = sourceSans,
                        color = if (selectedTime == item) Color.White else textColor,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.align(Alignment.Center)
                    )
                }
            }
        }
        Spacer(modifier = Modifier.height(48.dp))
        AppButton(
            text = "Подтвердить",
            modifier = Modifier.fillMaxWidth()
        ) {
            val timeSplit = selectedTime.split(":")

            onTimeConfirm("$dateText $selectedTime", LocalDateTime.of(mYear, mMonth, mDay, timeSplit[0].toInt(), timeSplit[1].toInt()))
        }
    }
}

/*
Описание: Окно выбора новых пациентов
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@Composable
fun PatientBottomSheet(
    userList: MutableList<UserCard>,
    onPatientConfirm: (UserCard) -> Unit,
    onBackPressed: () -> Unit
) {
    val mContext = LocalContext.current
    var selectedUser: UserCard? by remember { mutableStateOf<UserCard?>(null) }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .verticalScroll(rememberScrollState())
            .padding(horizontal = 20.dp)
            .padding(top = 24.dp, bottom = 32.dp)
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = "Выбор пациента",
                fontSize = 20.sp,
                fontWeight = FontWeight.SemiBold,
                fontFamily = sourceSans
            )
            AppDismissButton {
                onBackPressed()
            }
        }
        Spacer(modifier = Modifier.height(24.dp))
        for (user in userList.distinct()) {
            Card(
                elevation = 0.dp,
                backgroundColor = if (selectedUser == user) primaryColor else inputColor,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 16.dp)
                    .clickable {
                        selectedUser = user
                    }
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(12.dp)
                ) {
                    Image(
                        painter = if (user.pol == "Мужской") painterResource(id = R.drawable.ic_male) else painterResource(id = R.drawable.ic_female),
                        contentDescription = "",
                        modifier = Modifier.size(24.dp)
                    )
                    Spacer(modifier = Modifier.width(12.dp))
                    Text(
                        text = "${user.lastname} ${user.firstname}",
                        fontSize = 16.sp,
                        fontFamily = sourceSans,
                        color = if (selectedUser == user) Color.White else Color.Black
                    )
                }
            }
        }
        Spacer(modifier = Modifier.height(8.dp))
        AppButton(
            text = "Добавить пациента",
            fontSize = 15.sp,
            fontWeight = FontWeight.Normal,
            color = primaryColor,
            colors = ButtonDefaults.buttonColors(backgroundColor = Color.White),
            borderStroke = BorderStroke(1.dp, primaryColor),
            contentPadding = PaddingValues(14.dp),
            modifier = Modifier.fillMaxWidth()
        ) {
            val intent = Intent(mContext, CreateCardActivity::class.java)
            mContext.startActivity(intent)
        }
        Spacer(modifier = Modifier.height(40.dp))
        AppButton(
            text = "Подтвердить",
            enabled = selectedUser != null,
            modifier = Modifier.fillMaxWidth()
        ) {
            onPatientConfirm(selectedUser!!)
        }
    }
}