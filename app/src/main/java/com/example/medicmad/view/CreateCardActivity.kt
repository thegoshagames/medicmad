package com.example.medicmad.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.lifecycle.ViewModelProvider
import com.example.medicmad.R
import com.example.medicmad.common.DataSaver
import com.example.medicmad.common.UserService
import com.example.medicmad.model.UserCard
import com.example.medicmad.view.ui.components.AppButton
import com.example.medicmad.view.ui.components.AppTextButton
import com.example.medicmad.view.ui.components.AppTextField
import com.example.medicmad.view.ui.theme.*
import com.example.medicmad.viewmodel.LoginViewModel

/*
Описание: Класс экрана создания карты пациента
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
class CreateCardActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MedicMADTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    CardContent()
                }
            }
        }
    }

    /*
    Описание: Контент экрана создания карты пациента
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    @OptIn(ExperimentalMaterialApi::class)
    @Composable
    fun CardContent() {
        val mContext = LocalContext.current
        val shared = this.getSharedPreferences("shared", Context.MODE_PRIVATE)

        val userList = UserService().getUserList(shared)

        val viewModel = ViewModelProvider(this)[LoginViewModel::class.java]

        var enabled by rememberSaveable { mutableStateOf(false) }

        var firstName by rememberSaveable { mutableStateOf("") }
        var patronymic by rememberSaveable { mutableStateOf("") }
        var lastName by rememberSaveable { mutableStateOf("") }
        var birthday by rememberSaveable { mutableStateOf("") }
        var gender by rememberSaveable { mutableStateOf("") }

        var isLoading by rememberSaveable { mutableStateOf(false) }
        var isAlertVisible by rememberSaveable { mutableStateOf(false) }

        var isDropdownOpened by rememberSaveable { mutableStateOf(false) }

        val user by viewModel.user.observeAsState()
        LaunchedEffect(user) {
            isLoading = false

            if (user != null) {
                userList.add(user!!)

                UserService().saveUser(shared, userList)
            }
        }

        val errorMessage by viewModel.errorMessage.observeAsState()
        LaunchedEffect(errorMessage) {
            isLoading = false

            if (errorMessage != null) {
                isAlertVisible = true
            }
        }

        Scaffold(
            topBar = {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 20.dp)
                        .padding(top = 32.dp)
                ) {
                    Text(
                        text = "Создание карты пациента",
                        fontSize = 24.sp,
                        fontWeight = FontWeight.Bold,
                        fontFamily = sourceSans,
                        modifier = Modifier.fillMaxWidth(0.6f)
                    )
                    AppTextButton(
                        text = "Пропустить",
                        fontSize = 15.sp,
                        fontWeight = FontWeight.Normal,
                        fontFamily = sourceSans,
                        color = primaryColor,
                        modifier = Modifier.padding(top = 10.dp)
                    ) {
                        val intent = Intent(mContext, HomeActivity::class.java)
                        startActivity(intent)
                    }
                }
            }
        ) { padding ->
            Box(modifier = Modifier
                .padding(padding)
                .fillMaxSize()) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 20.dp)
                        .verticalScroll(rememberScrollState())
                ) {
                    Column(
                        modifier = Modifier
                            .widthIn(max = 400.dp)
                            .fillMaxWidth()
                    ) {
                        Spacer(modifier = Modifier.height(16.dp))
                        Text(
                            text = "Без карты пациента вы не сможете заказать анализы.",
                            fontSize = 14.sp,
                            fontFamily = sourceSans,
                            color = descriptionColor
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(
                            text = "В картах пациентов будут храниться результаты анализов вас и ваших близких.",
                            fontSize = 14.sp,
                            fontFamily = sourceSans,
                            color = descriptionColor
                        )
                        Spacer(modifier = Modifier.height(32.dp))
                        AppTextField(
                            value = firstName,
                            onValueChange = {
                                firstName = it

                                enabled = firstName.isNotEmpty() && patronymic.isNotEmpty() && lastName.isNotEmpty() && birthday.isNotEmpty() && gender.isNotEmpty()
                            },
                            modifier = Modifier.fillMaxWidth(),
                            placeholder = {
                                Text(
                                    text = "Имя",
                                    fontSize = 15.sp,
                                    fontFamily = sourceSans,
                                    color = descriptionColor
                                )
                            },
                            stroke = if (firstName.isNotEmpty()) iconColor else strokeColor
                        )
                        Spacer(modifier = Modifier.height(24.dp))
                        AppTextField(
                            value = patronymic,
                            onValueChange = {
                                patronymic = it

                                enabled = firstName.isNotEmpty() && patronymic.isNotEmpty() && lastName.isNotEmpty() && birthday.isNotEmpty() && gender.isNotEmpty()
                            },
                            modifier = Modifier.fillMaxWidth(),
                            placeholder = {
                                Text(
                                    text = "Отчество",
                                    fontSize = 15.sp,
                                    fontFamily = sourceSans,
                                    color = descriptionColor
                                )
                            },
                            stroke = if (patronymic.isNotEmpty()) iconColor else strokeColor
                        )
                        Spacer(modifier = Modifier.height(24.dp))
                        AppTextField(
                            value = lastName,
                            onValueChange = {
                                lastName = it

                                enabled = firstName.isNotEmpty() && patronymic.isNotEmpty() && lastName.isNotEmpty() && birthday.isNotEmpty() && gender.isNotEmpty()
                            },
                            modifier = Modifier.fillMaxWidth(),
                            placeholder = {
                                Text(
                                    text = "Фамилия",
                                    fontSize = 15.sp,
                                    fontFamily = sourceSans,
                                    color = descriptionColor
                                )
                            },
                            stroke = if (lastName.isNotEmpty()) iconColor else strokeColor
                        )
                        Spacer(modifier = Modifier.height(24.dp))
                        AppTextField(
                            value = birthday,
                            onValueChange = {
                                birthday = it

                                enabled = firstName.isNotEmpty() && patronymic.isNotEmpty() && lastName.isNotEmpty() && birthday.isNotEmpty() && gender.isNotEmpty()
                            },
                            modifier = Modifier.fillMaxWidth(),
                            placeholder = {
                                Text(
                                    text = "Дата рождения",
                                    fontSize = 15.sp,
                                    fontFamily = sourceSans,
                                    color = descriptionColor
                                )
                            },
                            stroke = if (birthday.isNotEmpty()) iconColor else strokeColor
                        )
                        Spacer(modifier = Modifier.height(24.dp))
                        ExposedDropdownMenuBox(expanded = isDropdownOpened, onExpandedChange = { isDropdownOpened = !isDropdownOpened }) {
                            AppTextField(
                                value = gender,
                                onValueChange = {
                                    gender = it

                                    enabled = firstName.isNotEmpty() && patronymic.isNotEmpty() && lastName.isNotEmpty() && birthday.isNotEmpty() && gender.isNotEmpty()
                                },
                                modifier = Modifier.fillMaxWidth(),
                                placeholder = {
                                    Text(
                                        text = "Пол",
                                        fontSize = 15.sp,
                                        fontFamily = sourceSans,
                                        color = descriptionColor
                                    )
                                },
                                trailingIcon = {
                                    Icon(
                                        painter = painterResource(id = R.drawable.ic_drop),
                                        contentDescription = "",
                                        tint = textColor,
                                        modifier = Modifier.clickable {

                                        }
                                    )
                                }
                            )
                            DropdownMenu(expanded = isDropdownOpened, onDismissRequest = { isDropdownOpened = !isDropdownOpened }) {
                                DropdownMenuItem(onClick = {
                                    gender = "Мужской"
                                    isDropdownOpened = false

                                    enabled = firstName.isNotEmpty() && patronymic.isNotEmpty() && lastName.isNotEmpty() && birthday.isNotEmpty() && gender.isNotEmpty()
                                }) {
                                    Text(text = "Мужской")
                                }
                                DropdownMenuItem(onClick = {
                                    gender = "Женский"
                                    isDropdownOpened = false

                                    enabled = firstName.isNotEmpty() && patronymic.isNotEmpty() && lastName.isNotEmpty() && birthday.isNotEmpty() && gender.isNotEmpty()
                                }) {
                                    Text(text = "Женский")
                                }
                            }
                        }
                        Spacer(modifier = Modifier.height(48.dp))
                        AppButton(
                            text = "Создать",
                            enabled = enabled,
                            modifier = Modifier.fillMaxWidth()
                        ) {
                            isLoading = true

                            viewModel.createCard(
                                DataSaver(application).getStringFromSharedPreferences("token"),
                                userCard = UserCard(
                                    firstname = firstName,
                                    middlename = patronymic,
                                    lastname = lastName,
                                    bith = birthday,
                                    pol = gender
                                )
                            )
                        }
                    }
                }
            }
        }

        if (isLoading) {
            Dialog(onDismissRequest = {}) {
                CircularProgressIndicator()
            }
        }

        if (isAlertVisible) {
            AlertDialog(
                onDismissRequest = {
                    isAlertVisible = false
                    viewModel.errorMessage.value = null
                },
                title = { Text("Ошибка") },
                text = { Text(viewModel.errorMessage.value.toString()) },
                buttons = {
                    TextButton(onClick = {
                        isAlertVisible = false
                        viewModel.errorMessage.value = null
                    }) {
                        Text(text = "OK")
                    }
                }
            )
        }
    }
}