package com.example.medicmad.view.ui.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.medicmad.R
import com.example.medicmad.model.CartItem
import com.example.medicmad.model.CatalogItem
import com.example.medicmad.view.ui.theme.*
import java.util.Locale.Category

/*
Описание: Компонент названия категории
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@Composable
fun CategoryComponent(
    title: String,
    isSelected: Boolean,
    onClick: () -> Unit
) {
    Card(
        backgroundColor = if (isSelected) primaryColor else inputColor,
        elevation = 0.dp,
        shape =  MaterialTheme.shapes.medium,
        modifier = Modifier.clickable { 
            onClick()
        }
    ) {
        Text(
            text = title,
            fontSize = 15.sp,
            fontWeight = FontWeight.Normal,
            color = if (isSelected) Color.White else textColor,
            modifier = Modifier.padding(horizontal = 20.dp, vertical = 14.dp)
        )
    }
}

/*
Описание: Контент блока "Категории товаров"
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@Composable
fun CatalogBlock(
    catalogItem: CatalogItem,
    itemInCart: Boolean,
    onCardClick: () -> Unit,
    onClick: () -> Unit,
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .shadow(10.dp, MaterialTheme.shapes.medium, spotColor = Color.Black.copy(0.2f))
            .clip(MaterialTheme.shapes.medium)
            .background(Color.White)
            .clickable { onCardClick() }
    ) {
        Column(modifier = Modifier.fillMaxWidth()) {
            Text(
                text = catalogItem.name,
                fontSize = 16.sp,
                fontWeight = FontWeight.Normal,
                fontFamily = sourceSans,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            )
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp)
                    .padding(bottom = 16.dp)
            ) {
                Column {
                    Text(
                        text = catalogItem.time_result,
                        fontSize = 14.sp,
                        fontWeight = FontWeight.SemiBold,
                        fontFamily = sourceSans,
                        color = descriptionColor
                    )
                    Spacer(modifier = Modifier.height(4.dp))
                    Text(
                        text = "${catalogItem.price} ₽",
                        fontSize = 17.sp,
                        fontWeight = FontWeight.SemiBold,
                        fontFamily = sourceSans
                    )
                }

                if (itemInCart) {
                    AppButton(
                        text = "Убрать",
                        color = primaryColor,
                        fontSize = 14.sp,
                        borderStroke = BorderStroke(1.dp, primaryColor),
                        contentPadding = PaddingValues(horizontal = 16.dp, vertical = 10.dp),
                        colors = ButtonDefaults.buttonColors(backgroundColor = Color.White)
                    ) {
                        onClick()
                    }
                } else {
                    AppButton(
                        text = "Добавить",
                        fontSize = 14.sp,
                        contentPadding = PaddingValues(horizontal = 16.dp, vertical = 10.dp)
                    ) {
                        onClick()
                    }
                }
            }
        }
    }
}

/*
Описание: Контент блока товара в корзине
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@Composable
fun CatalogCartBlock(
    cartItem: CartItem,
    onItemPlus: () -> Unit,
    onItemMinus: () -> Unit,
    onItemDelete: () -> Unit,
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .shadow(10.dp, MaterialTheme.shapes.medium, spotColor = Color.Black.copy(0.2f))
            .clip(MaterialTheme.shapes.medium)
            .background(Color.White)
    ) {
        Column(modifier = Modifier.fillMaxWidth()) {
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            ) {
                Text(
                    text = cartItem.name,
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Normal,
                    fontFamily = sourceSans,
                    modifier = Modifier
                        .fillMaxWidth(0.7f)
                )
                Icon(
                    painter = painterResource(id = R.drawable.ic_dismiss),
                    contentDescription = "",
                    tint = textColor,
                    modifier = Modifier.clickable {
                        onItemDelete()
                    }
                )
            }
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp)
                    .padding(bottom = 16.dp)
            ) {
                Text(
                    text = "${cartItem.price} ₽",
                    fontSize = 17.sp,
                    fontWeight = FontWeight.Normal,
                    fontFamily = sourceSans
                )

                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(text = "${cartItem.count} пациент")
                    Spacer(modifier = Modifier.width(16.dp))
                    Card(
                        shape = MaterialTheme.shapes.small,
                        elevation = 0.dp,
                        backgroundColor = inputColor
                    ) {
                        Row(modifier = Modifier.height(IntrinsicSize.Max)) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_minus),
                                contentDescription = "",
                                tint = if (cartItem.count > 1) descriptionColor else iconColor,
                                modifier = Modifier.padding(6.dp).clickable(cartItem.count > 1) {
                                    onItemMinus()
                                }
                            )
                            Spacer(modifier = Modifier.width(1.dp).fillMaxHeight().background(strokeColor))
                            Icon(
                                painter = painterResource(id = R.drawable.ic_plus),
                                contentDescription = "",
                                tint = descriptionColor,
                                modifier = Modifier.padding(6.dp).clickable {
                                    onItemPlus()
                                }
                            )
                        }
                    }
                }
            }
        }
    }
}