package com.example.medicmad.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.lifecycle.ViewModelProvider
import com.example.medicmad.R
import com.example.medicmad.common.AddressService
import com.example.medicmad.common.CartService
import com.example.medicmad.common.DataSaver
import com.example.medicmad.common.UserService
import com.example.medicmad.model.*
import com.example.medicmad.view.ui.components.*
import com.example.medicmad.view.ui.theme.*
import com.example.medicmad.viewmodel.LoginViewModel
import com.example.medicmad.viewmodel.OrderViewModel
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.launch

/*
Описание: Класс экрана оформления заказа
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
class OrderActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MedicMADTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    OrderContent()
                }
            }
        }
    }

    /*
    Описание: Контент экрана оформления заказа
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    @SuppressLint("CoroutineCreationDuringComposition")
    @OptIn(ExperimentalMaterialApi::class)
    @Composable
    fun OrderContent() {
        val mContext = LocalContext.current
        val shared = this.getSharedPreferences("shared", Context.MODE_PRIVATE)

        val viewModel = ViewModelProvider(this)[OrderViewModel::class.java]

        var isLoading by rememberSaveable { mutableStateOf(false) }
        var isAlertVisible by rememberSaveable { mutableStateOf(false) }

        var enabled by rememberSaveable { mutableStateOf(false) }

        var userToChange: UserCard? by remember { mutableStateOf(null) }

        var cart: MutableList<CartItem> = remember { mutableStateListOf() }
        val userList: MutableList<UserCard> = remember { mutableStateListOf() }
        val selectedUserList: MutableList<UserCard> = remember { mutableStateListOf() }

        val addressList: MutableList<AddressItem> = remember { mutableStateListOf() }

        var selectedBottomSheet by rememberSaveable { mutableStateOf(2) }
        val bottomSheetState = rememberModalBottomSheetState(initialValue = ModalBottomSheetValue.Hidden, skipHalfExpanded = true)
        val scope = rememberCoroutineScope()

        val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mContext)

        val addressInteraction = remember { MutableInteractionSource() }
        if (addressInteraction.collectIsPressedAsState().value) {
            isLoading = true

            viewModel.searchAddress(this@OrderActivity, mContext, fusedLocationProviderClient)
        }

        val timeInteraction = remember { MutableInteractionSource() }
        if (timeInteraction.collectIsPressedAsState().value) {
            selectedBottomSheet = 1

            scope.launch {
                bottomSheetState.show()
            }
        }

        val patientInteraction = remember { MutableInteractionSource() }
        if (patientInteraction.collectIsPressedAsState().value) {
            userToChange = selectedUserList[0]
            selectedBottomSheet = 2

            scope.launch {
                bottomSheetState.show()
            }
        }

        var addressText by rememberSaveable { mutableStateOf("") }
        var timeText by rememberSaveable { mutableStateOf("") }
        var phoneText by rememberSaveable { mutableStateOf("") }
        var commentText by rememberSaveable { mutableStateOf("") }

        LaunchedEffect(Unit) {
            cart.addAll(CartService().getCart(shared))
            userList.addAll(UserService().getUserList(shared))
            addressList.addAll(AddressService().getAddressList(shared))

            for (c in cart.distinct()) {
                c.count = 1
            }

            if (userList.isNotEmpty()) {
                selectedUserList.add(userList[0])

                selectedUserList[0].userCart!!.addAll(cart)
            }
        }

        val address by viewModel.address.observeAsState()
        LaunchedEffect(address) {
            isLoading = false

            if (address != null) {
                selectedBottomSheet = 0

                scope.launch {
                    bottomSheetState.show()
                }
            }
        }

        val response by viewModel.responseCode.observeAsState()
        LaunchedEffect(response) {
            isLoading = false

            if (response == 200) {
                val intent = Intent(mContext, SuccessActivity::class.java)
                startActivity(intent)
            }
        }

        val errorMessage by viewModel.errorMessage.observeAsState()
        LaunchedEffect(errorMessage) {
            isLoading = false

            if (errorMessage != null) {
                isAlertVisible = true
            }
        }

        ModalBottomSheetLayout(
            sheetShape = RoundedCornerShape(topStart = 24.dp, topEnd = 24.dp),
            sheetState = bottomSheetState,
            sheetContent ={
                when (selectedBottomSheet) {
                    0 -> {
                        AddressBottomSheet(
                            address = address.toString(),
                            lat = viewModel.lat.value.toString(),
                            lng = viewModel.long.value.toString(),
                            alt = viewModel.alt.value.toString(),
                            onAddressSave = {
                                addressList.add(it)

                                AddressService().saveAddressList(shared, addressList)
                            },
                            onAddressConfirm = {
                                addressText = it.addressText

                                enabled = addressText.isNotEmpty() && timeText.isNotEmpty() && phoneText.isNotEmpty()
                            }
                        )
                    }
                    1 -> {
                        TimeBottomSheet(
                            onTimeConfirm = { time, localDateTime ->
                                timeText = time

                                enabled = addressText.isNotEmpty() && timeText.isNotEmpty() && phoneText.isNotEmpty()
                            },
                            onBackPressed = {
                                scope.launch {
                                    bottomSheetState.hide()
                                }
                            }
                        )
                    }
                    2 -> {
                        PatientBottomSheet(
                            userList = userList,
                            onPatientConfirm = { user ->
                                if (userToChange != null) {
                                    val userIndex = selectedUserList.indexOfFirst { it.firstname == user.firstname && it.lastname == user.lastname && it.bith == user.bith }
                                    val userToChangeIndex = selectedUserList.indexOfFirst { it.firstname == userToChange!!.firstname && it.lastname == userToChange!!.lastname && it.bith == userToChange!!.bith }

                                    if (userToChangeIndex != -1) {
                                        if (userIndex == -1) {
                                            selectedUserList.removeAt(userToChangeIndex)
                                            selectedUserList.add(userToChangeIndex, user)
                                        }
                                    }
                                } else {
                                    user.userCart!!.clear()
                                    user.userCart!!.addAll(cart)

                                    selectedUserList.add(user)
                                }
                            },
                            onBackPressed = {
                                scope.launch {
                                    bottomSheetState.hide()
                                }
                            }
                        )
                    }
                }
            }
        ) {
            Scaffold(
                topBar = {
                    AppBackButton(
                        modifier = Modifier.padding(start = 20.dp, top = 16.dp, bottom = 24.dp)
                    ) {
                        val intent = Intent(mContext, HomeActivity::class.java)
                        startActivity(intent)
                    }
                }
            ) { padding ->
                Box(modifier = Modifier.padding(padding)) {
                    Column(modifier = Modifier
                        .fillMaxWidth()
                        .verticalScroll(rememberScrollState())
                    ) {
                        Column(modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 20.dp)) {
                            Row(
                                horizontalArrangement = Arrangement.SpaceBetween,
                                modifier = Modifier
                                    .fillMaxWidth()
                            ) {
                                Text(
                                    text = "Оформление заказа",
                                    fontSize = 24.sp,
                                    fontWeight = FontWeight.Bold,
                                    fontFamily = sourceSans
                                )
                            }
                            Spacer(modifier = Modifier.height(32.dp))
                            Text(
                                text = "Адрес *",
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Normal,
                                fontFamily = sourceSans,
                                color = textColor
                            )
                            Spacer(modifier = Modifier.height(4.dp))
                            AppTextField(
                                value = addressText,
                                onValueChange = {},
                                readOnly = true,
                                placeholder = {
                                    Text(
                                        text = "Введите ваш адрес",
                                        fontSize = 15.sp,
                                        fontFamily = sourceSans,
                                        fontWeight = FontWeight.Normal
                                    )
                                },
                                modifier = Modifier.fillMaxWidth(),
                                interactionSource = addressInteraction
                            )
                            Spacer(modifier = Modifier.height(16.dp))
                            Text(
                                text = "Дата и время*",
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Normal,
                                fontFamily = sourceSans,
                                color = textColor
                            )
                            Spacer(modifier = Modifier.height(4.dp))
                            AppTextField(
                                value = timeText,
                                onValueChange = {},
                                readOnly = true,
                                placeholder = {
                                    Text(
                                        text = "Выберите дату и время",
                                        fontSize = 14.sp,
                                        fontFamily = sourceSans,
                                        fontWeight = FontWeight.Normal,
                                        color = descriptionColor
                                    )
                                },
                                modifier = Modifier.fillMaxWidth(),
                                interactionSource = timeInteraction
                            )
                            Spacer(modifier = Modifier.height(32.dp))
                            Text(
                                text = buildAnnotatedString {
                                    append("Кто будет сдавать анализы? ")
                                    
                                    withStyle(SpanStyle(color = Color(0xFFFD3535))) {
                                        append("*")
                                    }
                                },
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Normal,
                                fontFamily = sourceSans,
                                color = textColor
                            )
                            Spacer(modifier = Modifier.height(4.dp))
                            if (selectedUserList.size > 1) {
                                for (user in selectedUserList.distinct()) {
                                    PatientOrderCard(
                                        cart = cart,
                                        user = user,
                                        onUserChange = {
                                            userToChange = it

                                            selectedBottomSheet = 2

                                            scope.launch {
                                                bottomSheetState.show()
                                            }
                                        },
                                        onUserRemove = {
                                            selectedUserList.remove(it)
                                        },
                                        onUserSave = { u ->
                                            val index = selectedUserList.indexOfFirst { it.firstname == u.firstname && it.lastname == u.lastname && it.bith == u.bith }

                                            if (index != -1) {
                                                selectedUserList.removeAt(index)
                                                selectedUserList.add(index, u)
                                            }
                                        }
                                    )
                                }
                            } else {
                                if (selectedUserList.isNotEmpty()) {
                                    AppTextField(
                                        value = "${selectedUserList[0].lastname} ${selectedUserList[0].firstname}",
                                        onValueChange = {},
                                        readOnly = true,
                                        placeholder = {
                                            Text(
                                                text = "",
                                                fontSize = 14.sp,
                                                fontFamily = sourceSans,
                                                fontWeight = FontWeight.Normal,
                                                color = descriptionColor
                                            )
                                        },
                                        leadingIcon = {
                                            Image(
                                                painter = if (selectedUserList[0].pol == "Мужской") painterResource(id = R.drawable.ic_male) else painterResource(id = R.drawable.ic_female),
                                                contentDescription = "",
                                                modifier = Modifier.size(24.dp)
                                            )
                                        },
                                        trailingIcon = {
                                            Icon(
                                                painter = painterResource(id = R.drawable.ic_drop),
                                                contentDescription = "",
                                                tint = textColor
                                            )
                                        },
                                        modifier = Modifier.fillMaxWidth(),
                                        interactionSource = patientInteraction
                                    )
                                }
                                Spacer(modifier = Modifier.height(16.dp))
                            }
                            AppButton(
                                text = "Добавить еще пациента",
                                fontSize = 15.sp,
                                fontWeight = FontWeight.Normal,
                                color = primaryColor,
                                colors = ButtonDefaults.buttonColors(backgroundColor = Color.White),
                                borderStroke = BorderStroke(1.dp, primaryColor),
                                contentPadding = PaddingValues(14.dp),
                                modifier = Modifier.fillMaxWidth()
                            ) {
                                selectedBottomSheet = 2

                                scope.launch {
                                    bottomSheetState.show()
                                }
                            }
                            Spacer(modifier = Modifier.height(32.dp))
                            Text(
                                text = "Телефон *",
                                fontSize = 14.sp,
                                fontWeight = FontWeight.Normal,
                                fontFamily = sourceSans,
                                color = textColor
                            )
                            Spacer(modifier = Modifier.height(4.dp))
                            AppTextField(
                                value = phoneText,
                                onValueChange = {
                                    phoneText = it

                                    enabled = addressText.isNotEmpty() && timeText.isNotEmpty() && phoneText.isNotEmpty()
                                },
                                placeholder = {
                                    Text(
                                        text = "",
                                        fontSize = 14.sp,
                                        fontFamily = sourceSans,
                                        fontWeight = FontWeight.Normal,
                                        color = descriptionColor
                                    )
                                },
                                modifier = Modifier.fillMaxWidth(),
                            )
                            Spacer(modifier = Modifier.height(16.dp))
                            Row(
                                horizontalArrangement = Arrangement.SpaceBetween,
                                modifier = Modifier.fillMaxWidth()
                            ) {
                                Text(
                                    text = "Комментарий",
                                    fontSize = 14.sp,
                                    fontWeight = FontWeight.Normal,
                                    fontFamily = sourceSans,
                                    color = textColor
                                )
                                Image(
                                    painter = painterResource(id = R.drawable.ic_audio),
                                    contentDescription = "",
                                    modifier = Modifier.size(20.dp)
                                )
                            }
                            Spacer(modifier = Modifier.height(4.dp))
                            AppTextField(
                                value = commentText,
                                onValueChange = {
                                    commentText = it


                                },
                                placeholder = {
                                    Text(
                                        text = "",
                                        fontSize = 14.sp,
                                        fontFamily = sourceSans,
                                        fontWeight = FontWeight.Normal,
                                        color = descriptionColor
                                    )
                                },
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .height(128.dp),
                            )
                        }
                        Spacer(modifier = Modifier.height(47.dp))
                        Column(modifier = Modifier
                            .background(inputColor)
                            .fillMaxWidth()
                            .padding(20.dp)
                            .padding(top = 16.dp, bottom = 32.dp)
                        ) {
                            Row(
                                horizontalArrangement = Arrangement.SpaceBetween,
                                modifier = Modifier.fillMaxWidth()
                            ) {
                                Text(
                                    text = "Оплата Apple Pay",
                                    fontSize = 15.sp,
                                    fontFamily = sourceSans
                                )
                                Icon(
                                    painter = painterResource(id = R.drawable.ic_next),
                                    contentDescription = "",
                                    tint = iconColor
                                )
                            }
                            Spacer(modifier = Modifier.height(16.dp))
                            Row(
                                horizontalArrangement = Arrangement.SpaceBetween,
                                modifier = Modifier.fillMaxWidth()
                            ) {
                                Text(
                                    text = "Промокод",
                                    fontSize = 15.sp,
                                    fontFamily = sourceSans,
                                    color = descriptionColor
                                )
                                Icon(
                                    painter = painterResource(id = R.drawable.ic_next),
                                    contentDescription = "",
                                    tint = iconColor
                                )
                            }
                            Spacer(modifier = Modifier.height(29.dp))
                            Row(
                                horizontalArrangement = Arrangement.SpaceBetween,
                                modifier = Modifier.fillMaxWidth()
                            ) {
                                var cartSumPrice = 0
                                var itemCount = 0

                                for (user in selectedUserList.distinct()) {
                                    for (c in user.userCart!!.distinct()) {
                                        cartSumPrice += c.price.toInt() * c.count
                                        itemCount += c.count
                                    }
                                }

                                Text(
                                    text = "$itemCount анализ",
                                    fontSize = 17.sp,
                                    fontFamily = sourceSans,
                                )
                                Text(
                                    text = "$cartSumPrice ₽",
                                    fontSize = 17.sp,
                                    fontFamily = sourceSans,
                                )
                            }
                            Spacer(modifier = Modifier.height(12.dp))
                            AppButton(
                                text = "Заказать",
                                enabled = enabled,
                                modifier = Modifier.fillMaxWidth()
                            ) {
                                val patientList = ArrayList<OrderPatient>()

                                for (patient in selectedUserList.distinct()) {
                                    val itemList = ArrayList<OrderItem>()

                                    for (item in patient.userCart!!.distinct()) {
                                        itemList.add(
                                            OrderItem(
                                                item.id,
                                                item.price
                                            )
                                        )
                                    }

                                    patientList.add(
                                        OrderPatient(
                                            "${patient.lastname} ${patient.firstname}",
                                            itemList
                                        )
                                    )
                                }

                                val order = Order(
                                    addressText,
                                    timeText,
                                    phoneText,
                                    commentText,
                                    "",
                                    patientList
                                )

                                viewModel.createOrder(DataSaver(application).getStringFromSharedPreferences("token"), order)
                            }
                        }
                    }
                }
            }
        }

        if (isLoading) {
            Dialog(onDismissRequest = {}) {
                CircularProgressIndicator()
            }
        }

        if (isAlertVisible) {
            AlertDialog(
                onDismissRequest = {
                    isAlertVisible = false
                    viewModel.errorMessage.value = null
                },
                title = { Text("Ошибка") },
                text = { Text(viewModel.errorMessage.value.toString()) },
                buttons = {
                    TextButton(onClick = {
                        isAlertVisible = false
                        viewModel.errorMessage.value = null
                    }) {
                        Text(text = "OK")
                    }
                }
            )
        }
    }
}