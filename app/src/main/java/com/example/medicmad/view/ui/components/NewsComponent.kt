package com.example.medicmad.view.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.bumptech.glide.Glide
import com.example.medicmad.view.ui.theme.sourceSans
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage

/*
Описание: Компонент блока "Акции и новости"
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
@Composable
fun NewsComponent(
    title: String,
    text: String,
    cost: String,
    image: String
) {
    Box(
        modifier = Modifier
            .size(270.dp, 152.dp)
            .clip(MaterialTheme.shapes.large)
            .background(
                Brush.linearGradient(
                    listOf(
                        Color(0xFF76B3FF),
                        Color(0xFFCDE3FF)
                    )
                )
            )
    ) {
        GlideImage(
            imageModel = image,
            imageOptions = ImageOptions(Alignment.BottomEnd, contentScale = ContentScale.FillHeight),
            modifier = Modifier.align(Alignment.BottomEnd)
        )

        Text(
            text = title,
            fontSize = 18.sp,
            fontWeight = FontWeight.Bold,
            fontFamily = sourceSans,
            color = Color.White,
            modifier = Modifier
                .fillMaxWidth(0.8f)
                .padding(16.dp)
        )
        Column(
            modifier = Modifier
                .fillMaxWidth(0.6f)
                .align(Alignment.BottomStart)
                .padding(start = 16.dp, bottom = 12.dp)
        ) {
            Text(
                text = text,
                fontSize = 12.sp,
                fontFamily = sourceSans,
                lineHeight = 13.sp,
                color = Color.White
            )
            Text(
                text = "$cost ₽",
                fontSize = 18.sp,
                fontFamily = sourceSans,
                fontWeight = FontWeight.Bold,
                color = Color.White
            )
        }
    }
}