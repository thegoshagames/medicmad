package com.example.medicmad.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.lifecycle.ViewModelProvider
import com.example.medicmad.common.DataSaver
import com.example.medicmad.view.ui.components.AppBackButton
import com.example.medicmad.view.ui.components.AppTextField
import com.example.medicmad.view.ui.theme.MedicMADTheme
import com.example.medicmad.view.ui.theme.descriptionColor
import com.example.medicmad.view.ui.theme.sourceSans
import com.example.medicmad.viewmodel.LoginViewModel
import kotlinx.coroutines.delay

/*
Описание: Класс экрана проверки кода из почты
Дата создания: 04.04.2023
Автор: Георгий Хасанов
 */
class CodeActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MedicMADTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    CodeContent()
                }
            }
        }
    }

    /*
    Описание: Контент экрана проверки кода из почты
    Дата создания: 04.04.2023
    Автор: Георгий Хасанов
     */
    @Composable
    fun CodeContent() {
        val mContext = LocalContext.current

        val viewModel = ViewModelProvider(this)[LoginViewModel::class.java]

        var emailText by rememberSaveable { mutableStateOf(intent.getStringExtra("email").toString()) }

        var code1 by rememberSaveable { mutableStateOf("") }
        var code2 by rememberSaveable { mutableStateOf("") }
        var code3 by rememberSaveable { mutableStateOf("") }
        var code4 by rememberSaveable { mutableStateOf("") }

        var isLoading by rememberSaveable { mutableStateOf(false) }
        var isAlertVisible by rememberSaveable { mutableStateOf(false) }

        val response by viewModel.responseCode.observeAsState()
        LaunchedEffect(response) {
            if (response != null) {
                isLoading = false
            }
        }

        val token by viewModel.token.observeAsState()
        LaunchedEffect(token) {
            if (token != null) {
                isLoading = false

                DataSaver(application).saveStringToSharedPreferences("token", token.toString())

                val intent = Intent(mContext, PasswordActivity::class.java)
                startActivity(intent)
            }
        }

        val errorMessage by viewModel.errorMessage.observeAsState()
        LaunchedEffect(errorMessage) {
            if (errorMessage != null) {
                isLoading = false

                isAlertVisible = true
            }
        }

        var timer by rememberSaveable { mutableStateOf(59) }
        LaunchedEffect(timer) {
            delay(1000)

            if (timer > 0) {
                timer -= 1
            } else {
                isLoading = true

                viewModel.sendCode(emailText)

                timer = 59
            }
        }

        Scaffold(
            topBar = {
                AppBackButton(
                    modifier = Modifier.padding(start = 20.dp, top = 24.dp)
                ) {
                    onBackPressed()
                }
            }
        ) { padding ->
            Box(modifier = Modifier
                .fillMaxSize()
                .padding(padding)
            ) {
                Column(
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(Alignment.Center)
                ) {
                    Text(
                        "Введите код из E-mail",
                        fontSize = 17.sp,
                        fontWeight = FontWeight.SemiBold,
                        fontFamily = sourceSans,
                        textAlign = TextAlign.Center
                    )
                    Spacer(modifier = Modifier.height(28.dp))
                    Row(
                        horizontalArrangement = Arrangement.Center,
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier.widthIn(max = 350.dp).fillMaxWidth()
                    ) {
                        AppTextField(
                            value = code1,
                            onValueChange = {
                                if (it.length <= 1) {
                                    code1 = it
                                }
                            },
                            contentPadding = PaddingValues(0.dp),
                            textStyle = TextStyle(fontSize = 20.sp, fontFamily = sourceSans, lineHeight = 0.sp, textAlign = TextAlign.Center),
                            modifier = Modifier.size(48.dp)
                        )
                        Spacer(modifier = Modifier.width(16.dp))
                        AppTextField(
                            value = code2,
                            onValueChange = {
                                if (it.length <= 1) {
                                    code2 = it
                                }
                            },
                            contentPadding = PaddingValues(0.dp),
                            textStyle = TextStyle(fontSize = 20.sp, fontFamily = sourceSans, lineHeight = 0.sp, textAlign = TextAlign.Center),
                            modifier = Modifier.size(48.dp)
                        )
                        Spacer(modifier = Modifier.width(16.dp))
                        AppTextField(
                            value = code3,
                            onValueChange = {
                                if (it.length <= 1) {
                                    code3 = it
                                }
                            },
                            contentPadding = PaddingValues(0.dp),
                            textStyle = TextStyle(fontSize = 20.sp, fontFamily = sourceSans, lineHeight = 0.sp, textAlign = TextAlign.Center),
                            modifier = Modifier.size(48.dp)
                        )
                        Spacer(modifier = Modifier.width(16.dp))
                        AppTextField(
                            value = code4,
                            onValueChange = {
                                if (it.length <= 1) {
                                    code4 = it
                                }
                                
                                if (it.length == 1) {
                                    viewModel.checkCode(emailText, "$code1$code2$code3$code4")
                                }
                            },
                            contentPadding = PaddingValues(0.dp),
                            textStyle = TextStyle(fontSize = 20.sp, fontFamily = sourceSans, lineHeight = 0.sp, textAlign = TextAlign.Center),
                            modifier = Modifier.size(48.dp)
                        )
                    }
                    Spacer(modifier = Modifier.height(12.dp))
                    Text(
                        text = "Отправить код повторно можно будет через $timer секунд",
                        fontSize = 15.sp,
                        fontFamily = sourceSans,
                        color = descriptionColor,
                        textAlign = TextAlign.Center,
                        modifier = Modifier
                            .widthIn(max = 240.dp)
                            .fillMaxWidth()
                    )
                }
            }
        }

        if (isLoading) {
            Dialog(onDismissRequest = {}) {
                CircularProgressIndicator()
            }
        }

        if (isAlertVisible) {
            AlertDialog(
                onDismissRequest = {
                    isAlertVisible = false
                    viewModel.errorMessage.value = null
                },
                title = { Text("Ошибка") },
                text = { Text(viewModel.errorMessage.value.toString()) },
                buttons = {
                    TextButton(onClick = {
                        isAlertVisible = false
                        viewModel.errorMessage.value = null
                    }) {
                        Text(text = "OK")
                    }
                }
            )
        }
    }
}