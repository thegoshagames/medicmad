package com.example.medicmad

import android.content.Context
import androidx.activity.compose.setContent
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.medicmad.view.OnboardActivity

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Rule

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class OnboardTest {

    @get:Rule
    val onboardTestRule = createAndroidComposeRule<OnboardActivity>()

    //Изображение и текста из очереди извлекается правильно (в порядке добавления в очередь).
    @Test
    fun pagerTest() {
        onboardTestRule.activity.setContent {
            onboardTestRule.activity.ScreenContent()
        }

        onboardTestRule.onNodeWithText("Анализы").assertExists()
        onboardTestRule.onNodeWithText("Экспресс сбор и получение проб").assertExists()
        onboardTestRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        onboardTestRule.onNodeWithText("Уведомления").assertExists()
        onboardTestRule.onNodeWithText("Вы быстро узнаете о результатах").assertExists()
        onboardTestRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        onboardTestRule.onNodeWithText("Мониторинг").assertExists()
        onboardTestRule.onNodeWithText("Наши врачи всегда наблюдают за вашими показателями здоровья").assertExists()
    }

    // Корректное извлечение элементов из очереди (количество элементов в очереди уменьшается на единицу).
    @Test
    fun pagerCountTest() {
        onboardTestRule.activity.setContent {
            onboardTestRule.activity.ScreenContent()
        }

        onboardTestRule.onNodeWithText("Анализы").assertExists()
        onboardTestRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        onboardTestRule.onNodeWithText("Уведомления").assertExists()
        onboardTestRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        onboardTestRule.onNodeWithText("Мониторинг").assertExists()
    }

    // В случае, когда в очереди несколько картинок, устанавливается правильная надпись на кнопке.
    @Test
    fun buttonTextTest() {
        onboardTestRule.activity.setContent {
            onboardTestRule.activity.ScreenContent()
        }

        onboardTestRule.onNodeWithText("Пропустить").assertExists()
        onboardTestRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        onboardTestRule.onNodeWithText("Пропустить").assertExists()
        onboardTestRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        onboardTestRule.onNodeWithText("Завершить").assertExists()
    }

    // Случай, когда в очереди осталось только одно изображение, надпись на кнопке должна измениться на "Завершить".
    @Test
    fun endButtonTextTest() {
        onboardTestRule.activity.setContent {
            onboardTestRule.activity.ScreenContent()
        }

        onboardTestRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        onboardTestRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        onboardTestRule.onNodeWithText("Завершить").assertExists()
    }

    // Если очередь пустая и пользователь нажал на кнопку “Завершить”, происходит открытие экрана «Вход и регистрация/не заполнено» приложения. Если очередь не пустая – переход отсутствует.
    @Test
    fun gotoLoginTest() {
        onboardTestRule.activity.setContent {
            onboardTestRule.activity.ScreenContent()
        }

        onboardTestRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        onboardTestRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        onboardTestRule.onNodeWithText("Завершить").assertExists().performTouchInput { click() }

        onboardTestRule.onNodeWithTag("login").assertExists()
    }

    // Наличие вызова метода сохранения флага об успешном прохождении приветствия пользователем.
    @Test
    fun savedLoginTest() {
        onboardTestRule.activity.setContent {
            onboardTestRule.activity.ScreenContent()
        }

        onboardTestRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        onboardTestRule.onNodeWithTag("pager").performTouchInput { swipeLeft() }
        onboardTestRule.onNodeWithText("Завершить").assertExists().performTouchInput { click() }

        onboardTestRule.onNodeWithTag("login").assertExists()
        val isSaved = onboardTestRule.activity.getSharedPreferences("shared", Context.MODE_PRIVATE).getBoolean("isFirstEnter", true)

        assertEquals(isSaved, false)
    }
}